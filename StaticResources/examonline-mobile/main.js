import Vue from 'vue'
import App from './App'
import cuCustom from './colorui/components/cu-custom.vue'
import store from './store'
//import './permission'
Vue.config.productionTip = false

Vue.prototype.$store = store

App.mpType = 'app'
Vue.component('cu-custom',cuCustom)
const app = new Vue({
    store,
    ...App
})
app.$mount()
