import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        /**
         * 是否需要强制登录
         */
        forcedLogin: false,
        hasLogin: false,
        user: {}
    },
    mutations: {
        login(state, user) {
            state.user = user || '新用户';
            state.hasLogin = true;
        },
        logout(state) {
            state.user ={};
            state.hasLogin = false;
        }
    }
})

export default store
