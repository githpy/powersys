-- --------------------------------------------------------
-- 主机:                           172.18.0.16
-- 服务器版本:                        5.7.24 - MySQL Community Server (GPL)
-- 服务器操作系统:                      Linux
-- HeidiSQL 版本:                  9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 正在导出表  openexam.exam_department 的数据：~5 rows (大约)
DELETE FROM `exam_department`;
/*!40000 ALTER TABLE `exam_department` DISABLE KEYS */;
INSERT INTO `exam_department` (`id`, `name`) VALUES
	(1, '综合事务部'),
	(2, '操作部'),
	(3, '业务部'),
	(4, '公司领导'),
	(5, '公司领导');
/*!40000 ALTER TABLE `exam_department` ENABLE KEYS */;

-- 正在导出表  openexam.exam_exam 的数据：~1 rows (大约)
DELETE FROM `exam_exam`;
/*!40000 ALTER TABLE `exam_exam` DISABLE KEYS */;
INSERT INTO `exam_exam` (`id`, `time`, `title`, `stime`, `author`, `hascompletion`, `etime`, `invalid`, `display`, `name`, `describe`, `limit`, `projectid`, `examdetailsid`, `totalgoal`, `subproid`) VALUES
	(8, 60, '测试', '2019-08-06 11:43:56', NULL, 0, '2019-08-06 11:43:56', 1, 1, '测试', NULL, 100, NULL, 8, NULL, '6-');
/*!40000 ALTER TABLE `exam_exam` ENABLE KEYS */;

-- 正在导出表  openexam.exam_examdetails 的数据：~1 rows (大约)
DELETE FROM `exam_examdetails`;
/*!40000 ALTER TABLE `exam_examdetails` DISABLE KEYS */;
INSERT INTO `exam_examdetails` (`id`, `dxsize`, `dxxsize`, `pdsize`, `tksize`, `dxcustomscore`, `dxxcustomscore`, `pdcustomscore`, `wdcustomscore`, `tkcustomscore`) VALUES
	(8, 10, 10, 10, 0, 2, 2, 2, 5, 5);
/*!40000 ALTER TABLE `exam_examdetails` ENABLE KEYS */;

-- 正在导出表  openexam.exam_project 的数据：~2 rows (大约)
DELETE FROM `exam_project`;
/*!40000 ALTER TABLE `exam_project` DISABLE KEYS */;
INSERT INTO `exam_project` (`id`, `name`, `distinction`, `subjectid`) VALUES
	(6, '方案一', NULL, 6),
	(7, '管理层一', NULL, 7);
/*!40000 ALTER TABLE `exam_project` ENABLE KEYS */;

-- 正在导出表  openexam.exam_question 的数据：~4 rows (大约)
DELETE FROM `exam_question`;
/*!40000 ALTER TABLE `exam_question` DISABLE KEYS */;
INSERT INTO `exam_question` (`id`, `title`, `typeid`, `display`, `projectid`, `subproid`) VALUES
	(146, '单选', 1, 1, 7, '7-'),
	(147, '判断题目', 4, 1, NULL, '6-'),
	(148, '问答题目', 3, 1, NULL, '6-'),
	(149, '多选题目', 2, 1, NULL, '7-7');
/*!40000 ALTER TABLE `exam_question` ENABLE KEYS */;

-- 正在导出表  openexam.exam_questiontype 的数据：~4 rows (大约)
DELETE FROM `exam_questiontype`;
/*!40000 ALTER TABLE `exam_questiontype` DISABLE KEYS */;
INSERT INTO `exam_questiontype` (`id`, `name`, `goal`, `characteristic`) VALUES
	(1, '单选题', NULL, NULL),
	(2, '多选题', NULL, NULL),
	(3, '问答题', NULL, NULL),
	(4, '判断题', NULL, NULL);
/*!40000 ALTER TABLE `exam_questiontype` ENABLE KEYS */;

-- 正在导出表  openexam.exam_result 的数据：~177 rows (大约)
DELETE FROM `exam_result`;
/*!40000 ALTER TABLE `exam_result` DISABLE KEYS */;
INSERT INTO `exam_result` (`id`, `text`, `rightr`, `questionid`, `display`) VALUES
	(230, '为了加强安全生产工作', 1, 93, 1),
	(231, '防止和减少生产安全事故', 1, 93, 1),
	(232, '保障人民群众生命和财产安全', 1, 93, 1),
	(233, '促进经济社会持续健康发展', 1, 93, 1),
	(234, '有关法律、行政法规对消防安全和道路交通安全、铁路交通安全、水上交通安全、民用航空安全以及核与辐射安全、特种设备安全另有规定的，适用其规定。', 1, 94, 1),
	(235, '《安全生产法》适用于国内从事生产经营活动的单位的安全生产。', 1, 94, 1),
	(236, '在国内外从事生产经营活动的单位(以下统称生产经营单位)的安全生产，适用本法', 0, 94, 1),
	(237, '《安全生产法》只适用于公有制企业，不适用于私有制企业。', 0, 94, 1),
	(238, '用人单位的主要负责人和职业卫生管理人员应当接受职业卫生培训，遵守职业病防治法律、法规，依法组织本单位的职业病防治工作。', 1, 95, 1),
	(239, '用人单位应当对劳动者进行上岗前的职业卫生培训和在岗期间的定期职业卫生培训，普及职业卫生知识，督促劳动者遵守职业病防治法律、法规、规章和操作规程，指导劳动者正确使用职业病防护设备和个人使用的职业病防护用品。', 1, 95, 1),
	(240, '劳动者应当学习和掌握相关的职业卫生知识，增强职业病防范意识，遵守职业病防治法律、法规、规章和操作规程，正确使用、维护职业病防护设备和个人使用的职业病防护用品。', 1, 95, 1),
	(241, '劳动者不履行前款规定义务的，用人单位应当对其进行教育。', 1, 95, 1),
	(242, '发现职业病危害事故隐患没必要及时报告', 0, 95, 1),
	(243, '订立的劳动合同，应当载明有关保障从业人员劳动安全、防止职业危害的事项，以及依法为从业人员办理工伤保险的事项。', 1, 96, 1),
	(244, '生产经营单位应当对从业人员进行安全生产教育和培训，应当建立安全生产教育和培训档案，如实记录安全生产教育和培训的时间、内容、参加人员以及考核结果等情况。', 1, 96, 1),
	(245, '生产经营单位应当建立健全生产安全事故隐患排查治理制度，事故隐患排查治理情况应当如实记录，并向从业人员通报。', 1, 96, 1),
	(246, '生产经营单位必须为从业人员提供符合国家标准或者行业标准的劳动防护用品，并监督、教育从业人员按照使用规则佩戴、使用。', 1, 96, 1),
	(247, '从业人员不需接受安全生产教育和培训，掌握本职工作所需的安全生产知识，提高安全生产技能，增强事故预防和应急处理能力。', 0, 96, 1),
	(248, '为了预防、控制和消除职业病危害，防治职业病，保护劳动者健康及其相关权益，促进经济社会发展，根据宪法，制定本法。', 1, 97, 1),
	(249, '职业病防治工作坚持预防为主、防治结合的方针，建立用人单位负责、行政机关监管、行业自律、职工参与和社会监督的机制，实行分类管理、综合治理。', 1, 97, 1),
	(250, '用人单位应当依照法律、法规要求，严格遵守国家职业卫生标准，落实职业病预防措施，从源头上控制和消除职业病危害。', 1, 97, 1),
	(251, '用人单位的主要负责人和职业卫生管理人员应当接受职业卫生培训，遵守职业病防治法律、法规，依法组织本单位的职业病防治工作。', 1, 97, 1),
	(252, '本法适用于国内外的职业病防治活动，本法所称职业病，是指企业、事业单位和个体经济组织等用人单位的劳动者在职业活动中，因接触粉尘、放射性物质和其他有毒、有害因素而引起的疾病。', 0, 97, 1),
	(253, '设置或者指定职业卫生管理机构或者组织，配备专职或者兼职的职业卫生管理人员，负责本单位的职业病防治工作。', 1, 98, 1),
	(254, '制定职业病防治计划和实施方案，建立、健全职业卫生管理制度和操作规程。', 1, 98, 1),
	(255, '建立、健全职业卫生档案和劳动者健康监护档案。', 1, 98, 1),
	(256, '建立、健全工作场所职业病危害因素监测及评价制度，建立、健全职业病危害事故应急救援预案。', 1, 98, 1),
	(257, '用人单位必须采用有效的职业病防护设施，并为劳动者提供个人使用的职业病防护用品。', 1, 98, 1),
	(258, '产生职业病危害的用人单位，应当在醒目位置设置公告栏，公布有关职业病防治的规章制度、操作规程、职业病危害事故应急救援措施和工作场所职业病危害因素检测结果。', 1, 99, 1),
	(259, '对产生严重职业病危害的作业岗位，应当在其醒目位置，设置警示标识和中文警示说明。警示说明应当载明产生职业病危害的种类、后果、预防以及应急救治措施等内容。', 1, 99, 1),
	(260, '对可能发生急性职业损伤的有毒、有害工作场所，用人单位应当设置报警装置，配置现场急救用品、冲洗设备、应急撤离通道和必要的泄险区。', 1, 99, 1),
	(261, '对职业病防护设备、应急救援设施和个人使用的职业病防护用品，用人单位应当进行经常性的维护、检修，定期检测其性能和效果，确保其处于正常状态，不得擅自拆除或者停止使用。', 1, 99, 1),
	(262, '向用人单位提供可能产生职业病危害的设备的，应当提供中文说明书，并在设备的醒目位置设置警示标识和中文警示说明。警示说明应当载明设备性能、可能产生的职业病危害、安全操作和维护注意事项、职业病防护以及应急救治措施等内容。', 1, 99, 1),
	(263, '坚持科学发展观', 1, 100, 1),
	(264, '以人为本，坚持安全发展', 0, 100, 1),
	(265, '坚持安全第一、预防为主、综合治理的方针', 0, 100, 1),
	(266, '强化和落实生产经营单位的主体责任', 0, 100, 1),
	(267, '安全生产的法律、法规', 1, 101, 1),
	(268, '加强安全生产管理', 1, 101, 1),
	(269, '建立、健全安全生产责任制和安全生产规章制度', 1, 101, 1),
	(270, '改善安全生产条件，推进安全生产标准化建设', 1, 101, 1),
	(271, '国家标准', 1, 102, 1),
	(272, '或者行业标准规定的安全生产条件', 1, 102, 1),
	(273, '不具备安全生产条件的，不得从事生产经营活动', 1, 102, 1),
	(274, '安全生产管理制度', 0, 102, 1),
	(275, '建立、健全本单位安全生产责任制; 组织制定本单位安全生产规章制度和操作规程;', 1, 103, 1),
	(276, '保证本单位安全生产投入的有效实施;督促、检查本单位的安全生产工作，及时消除生产安全事故隐患;', 1, 103, 1),
	(277, '组织制定并实施本单位的生产安全事故应急救援预案;及时、如实报告生产安全事故。', 1, 103, 1),
	(278, '组织制定并实施本单位安全生产教育和培训计划。', 1, 103, 1),
	(279, '前款规定以外的其他生产经营单位，从业人员超过一百人的，应当设置安全生产管理机构', 1, 104, 1),
	(280, '或者配备专职安全生产管理人员;', 1, 104, 1),
	(281, '从业人员在一百人以下的，应当配备专职或者兼职的安全生产管理人员。', 1, 104, 1),
	(282, '从业人员在50人以下的，不用配备专职或者兼职的安全生产管理人员。', 0, 104, 1),
	(283, '生产安全事故隐患排查治理制度', 1, 105, 1),
	(284, '采取技术、管理措施，及时发现并消除事故隐患。', 1, 105, 1),
	(285, '事故隐患排查治理情况应当如实记录，并向从业人员通报。', 1, 105, 1),
	(286, '安全制度和操作规程', 1, 105, 1),
	(287, '劳动者订立劳动合同（含聘用合同）应当将工作过程中可能产生的职业病危害及其后果、职业病防护措施和待遇等如实告知劳动者，并在劳动合同中写明，不得隐瞒或者欺骗。因工作岗位或者工作内容变更，用人单位应当依照规定，向劳动者履行如实告知的义务，并协商变更原劳动合同相关条款。', 1, 106, 1),
	(288, '用人单位的主要负责人和职业卫生管理人员应当接受职业卫生培训，遵守职业病防治法律、法规，依法组织本单位的职业病防治工作。', 1, 106, 1),
	(289, '用人单位应当对劳动者进行上岗前的职业卫生培训和在岗期间的定期职业卫生培训，普及职业卫生知识，督促劳动者遵守职业病防治法律、法规、规章和操作规程，指导劳动者正确使用职业病防护设备和个人使用的职业病防护用品。', 1, 106, 1),
	(290, '劳动者应当学习和掌握相关的职业卫生知识，增强职业病防范意识，遵守职业病防治法律、法规、规章和操作规程，正确使用、维护职业病防护设备和个人使用的职业病防护用品，发现职业病危害事故隐患应当及时报告。劳动者不履行前款规定义务的，用人单位应当对其进行教育。', 1, 106, 1),
	(291, '用人单位应当为劳动者建立职业健康监护档案，并按照规定的期限妥善保存。职业健康监护档案应当包括劳动者的职业史、职业病危害接触史、职业健康检查结果和职业病诊疗等有关个人健康资料。劳动者离开用人单位时，有权索取本人职业健康监护档案复印件，用人单位应当如实、无偿提供，并在所提供的复印件上签章。', 1, 106, 1),
	(292, '应当对从业人员进行安全生产教育和培训。', 1, 107, 1),
	(293, '应当建立安全生产教育和培训档案，如实记录安全生产教育和培训的时间、内容、参加人员以及考核结果等情况。', 1, 107, 1),
	(294, '生产经营单位对重大危险源应当登记建档，进行定期检测、评估、监控，并制定应急预案，告知从业人员和相关人员在紧急情况下应当采取的应急措施。', 1, 107, 1),
	(295, '对被派遣劳动者进行岗位安全操作规程和安全操作技能的教育和培训。劳务派遣单位可不对被派遣劳动者进行必要的安全生产教育和培训。', 0, 107, 1),
	(296, '应当根据其生产、储存的危险化学品的种类和危险特性，在作业场所设置相应的监测、监控、通风、防晒、调温、防火、灭火、防爆、泄压、防毒、中和、防潮、防雷、防静电、防腐、防泄漏以及防护围堤或者隔离操作等安全设施、设备。', 1, 108, 1),
	(297, '并按照国家标准、行业标准或者国家有关规定对安全设施、设备进行经常性维护、保养，保证安全设施、设备的正常使用。', 1, 108, 1),
	(298, '应当在其作业场所和安全设施、设备上设置明显的安全警示标志。', 1, 108, 1),
	(299, '作业场所应设置通信、报警装置，不需保证处于适用状态。', 0, 108, 1),
	(300, '专用仓库、专用场地或者专用储存室（以下统称专用仓库）内，并由专人负责管理。', 1, 109, 1),
	(301, '剧毒化学品以及储存数量构成重大危险源的其他危险化学品，应当在专用仓库内单独存放，并实行双人收发、双人保管制度。', 1, 109, 1),
	(302, '危险化学品的储存方式、方法以及储存数量应当符合国家标准或者国家有关规定。', 1, 109, 1),
	(303, '储存危险化学品的单位应当建立危险化学品出入库核查、登记制度。', 1, 109, 1),
	(304, '对剧毒化学品以及储存数量构成重大危险源的其他危险化学品，储存单位应当将其储存数量、储存地点以及管理人员的情况，报所在地县级安监部门（在港区内储存的，报港口部门）和公安机关备案。', 1, 109, 1),
	(305, '有符合国家标准、行业标准的经营场所，储存危险化学品的，还应当有符合国家标准、行业标准的储存设施；', 1, 110, 1),
	(306, '从业人员经过专业技术培训并经考核合格，有专职安全管理人员；', 1, 110, 1),
	(307, '有健全的安全管理规章制度，符合单位规定的危险化学品事故应急预案和必要的应急救援器材、设备。', 1, 110, 1),
	(308, '委托人身份信息和完整准确的危险货物品名、联合国编号、危险性分类、包装、数量、应急措施及安全技术说明书等资料；', 1, 111, 1),
	(309, '危险性质不明的危险货物，应当提供具有相应资质的专业机构出具的危险货物危险特性鉴定技术报告。', 1, 111, 1),
	(310, '法律、行政法规规定必须办理有关手续后方可进行水路运输的危险货物，还应当办理相关手续，并向港口经营人提供相关证明材料。', 1, 111, 1),
	(311, '危险货物港口经营人应当制定本单位危险货物事故专项应急预案和现场处置方案，依法配备应急救援人员和必要的应急救援器材、设备，每年至少组织二次应急救援培训和演练并如实记录，根据演练结果对应急预案进行修订。', 1, 111, 1),
	(312, '分为 9 大类。', 1, 112, 1),
	(313, '碳酸钡属于6.1类危险品。', 1, 112, 1),
	(314, '黄磷可用水密封，重箱可堆存4层高。', 0, 112, 1),
	(315, '磷酸的主要危险性是剧毒。', 0, 112, 1),
	(316, '进入危险品堆场，可拍照和接打电话 。', 0, 112, 1),
	(317, '违法建设危险货物堆场。', 1, 113, 1),
	(318, '违规储存危险货物 。', 1, 113, 1),
	(319, '违法经营。', 1, 113, 1),
	(320, '安全隐患长期存在。', 1, 113, 1),
	(321, '作业人员知晓作业货种信息，机械设备完好，作业路线明确，必须按照规定使用个人防护用品。', 1, 114, 1),
	(322, '危险品泄露容易造成环保问题。', 1, 114, 1),
	(323, '危险品堆场管理要求定时巡查 、储备足量、有效消防应急物资，可半封闭和混堆存放。', 1, 114, 1),
	(324, '清点和检查作业人员。', 1, 115, 1),
	(325, '布置作业地点和工作要求.', 1, 115, 1),
	(326, '明确装卸质量要求、详细布置生产安全注意事项和安全措施要求。', 1, 115, 1),
	(327, '', 0, 116, 1),
	(328, '违章作业是指违反规章制度，不服从管理，冒险进行操作的行为。', 0, 116, 1),
	(329, '违反劳动纪律，包括工作纪律，如不服从管理和工作安排的行为。', 0, 116, 1),
	(330, '违反作业要求和规定，是“三违”行为。', 1, 116, 1),
	(331, '违章指挥，违章指挥是指生产指挥人员滥用职权，强迫命令工人违反规章制度，冒险进行操作的行为。', 0, 116, 1),
	(332, '由于钢丝绳、吊索、链子、吊具等的缺陷造成吊物坠落事故。', 0, 117, 1),
	(333, '因交通道路狭窄拥挤,或因矿料堆埋引起铁路、道路路况不良,造成车辆的交通事故。', 0, 117, 1),
	(334, '各种露天电器因雨露浸湿而漏电,可能发生人员触电事故。', 0, 117, 1),
	(335, '露天作业工人,夏天可能出现中暑事故。', 0, 117, 1),
	(336, '由于起重吊物起落摆动造成碰撞人员事故。', 1, 117, 1),
	(337, '违法或犯罪。', 0, 118, 1),
	(338, '自杀或自残。', 0, 118, 1),
	(339, '斗殴或酗酒', 0, 118, 1),
	(340, '在上下班途中，受到非本人主要责任的交通事故或者城市轨道交通、客运轮渡、火车事故伤害的。', 1, 118, 1),
	(341, '集中精神，提高警惕，小心谨慎心理。', 0, 119, 1),
	(342, '冒险心理，争强好胜，喜欢逞能', 1, 119, 1),
	(343, '设备上有安全装置，而开车时不使用。', 0, 120, 1),
	(344, '特种作业未取证者独立操作。', 0, 120, 1),
	(345, '任意开动非本工种设备。', 0, 120, 1),
	(346, '任意拆除设备的安全装置，不按规定使用吊索具', 0, 120, 1),
	(347, '管理因素。', 1, 120, 1),
	(348, '建筑物 。', 0, 121, 1),
	(349, '照明灯塔。', 0, 121, 1),
	(350, '大豆输运带下。', 0, 121, 1),
	(351, '起重机或起重物。', 1, 121, 1),
	(352, '按规定穿戴各种防护用品。', 0, 122, 1),
	(353, '发现设备安全防护装置缺损，向上级反映。', 0, 122, 1),
	(354, '忽视警告，冒险进入危险区域。', 1, 122, 1),
	(355, '水', 0, 123, 1),
	(356, '泡沫', 0, 123, 1),
	(357, '饮料', 0, 123, 1),
	(358, '管理因素。', 1, 123, 1),
	(359, '水', 0, 124, 1),
	(360, '泡沫', 0, 124, 1),
	(361, '饮料', 0, 124, 1),
	(362, '关闭车门，驾驶室内外均不准搭乘人', 1, 124, 1),
	(363, '疏忽大意、违章驾车。', 0, 125, 1),
	(364, '车况不良。', 0, 125, 1),
	(365, '道路环境条件差。', 0, 125, 1),
	(366, '管理因素。', 1, 125, 1),
	(367, '公司物资可以私自借他人使用', 0, 126, 1),
	(368, '驾驶人应当按照驾驶证载明的准驾车型驾驶机动车，驾驶机动车时，应当随身携带机动车驾驶证。', 1, 126, 1),
	(369, '公司物资可以私自借他人使用', 0, 127, 1),
	(370, '保证安全防护、信号、保险等装置齐全、灵敏、可靠', 1, 127, 1),
	(371, '对', 1, 128, 1),
	(372, '错', 0, 128, 1),
	(373, '对', 1, 129, 1),
	(374, '错', 0, 129, 1),
	(375, '对', 1, 130, 1),
	(376, '错', 0, 130, 1),
	(377, '对', 1, 131, 1),
	(378, '错', 0, 131, 1),
	(379, '对', 1, 132, 1),
	(380, '错', 0, 132, 1),
	(381, '对', 1, 133, 1),
	(382, '错', 0, 133, 1),
	(383, '对', 1, 134, 1),
	(384, '错', 0, 134, 1),
	(385, '对', 1, 135, 1),
	(386, '错', 0, 135, 1),
	(387, '错', 1, 136, 1),
	(388, '对', 0, 136, 1),
	(389, '错', 1, 137, 1),
	(390, '对', 0, 137, 1),
	(391, '错', 1, 138, 1),
	(392, '对', 0, 138, 1),
	(393, '错', 1, 139, 1),
	(394, '对', 0, 139, 1),
	(407, '10元', 0, 146, 1),
	(408, '100元', 0, 146, 1),
	(409, '500元', 0, 146, 1),
	(410, '500元', 1, 146, 1),
	(411, '对', 1, 147, 1),
	(412, '错', 0, 147, 1),
	(413, '5', 1, 149, 1),
	(414, '6', 1, 149, 1),
	(415, '1', 0, 149, 1),
	(416, '2', 0, 149, 1),
	(417, '3', 0, 149, 1),
	(418, '4', 0, 149, 1);
/*!40000 ALTER TABLE `exam_result` ENABLE KEYS */;

-- 正在导出表  openexam.exam_subject 的数据：~2 rows (大约)
DELETE FROM `exam_subject`;
/*!40000 ALTER TABLE `exam_subject` DISABLE KEYS */;
INSERT INTO `exam_subject` (`id`, `name`, `distinction`) VALUES
	(6, '员工', NULL),
	(7, '管理层', NULL);
/*!40000 ALTER TABLE `exam_subject` ENABLE KEYS */;

-- 正在导出表  openexam.exam_sysuser 的数据：~1 rows (大约)
DELETE FROM `exam_sysuser`;
/*!40000 ALTER TABLE `exam_sysuser` DISABLE KEYS */;
INSERT INTO `exam_sysuser` (`id`, `username`, `password`) VALUES
	(2, 'admin', 'admin');
/*!40000 ALTER TABLE `exam_sysuser` ENABLE KEYS */;

-- 正在导出表  openexam.exam_upq 的数据：~270 rows (大约)
DELETE FROM `exam_upq`;
/*!40000 ALTER TABLE `exam_upq` DISABLE KEYS */;
INSERT INTO `exam_upq` (`id`, `questionid`, `userpaperid`, `goal`, `checked`, `questiontypeid`, `useranswer`, `answer`) VALUES
	(361, 116, 36, NULL, NULL, 1, 'false', NULL),
	(362, 126, 36, NULL, NULL, 1, 'false', NULL),
	(363, 118, 36, 0, 1, 1, '337', NULL),
	(364, 117, 36, NULL, NULL, 1, 'false', NULL),
	(365, 119, 36, NULL, NULL, 1, 'false', NULL),
	(366, 121, 36, NULL, NULL, 1, 'false', NULL),
	(367, 120, 36, NULL, NULL, 1, 'false', NULL),
	(368, 123, 36, NULL, NULL, 1, 'false', NULL),
	(369, 122, 36, NULL, NULL, 1, 'false', NULL),
	(370, 125, 36, NULL, NULL, 1, 'false', NULL),
	(371, 101, 36, 0, NULL, 2, NULL, NULL),
	(372, 102, 36, 0, NULL, 2, NULL, NULL),
	(373, 111, 36, 0, NULL, 2, NULL, NULL),
	(374, 99, 36, 0, NULL, 2, NULL, NULL),
	(375, 103, 36, 0, NULL, 2, NULL, NULL),
	(376, 115, 36, 0, NULL, 2, NULL, NULL),
	(377, 104, 36, 0, NULL, 2, NULL, NULL),
	(378, 97, 36, 0, NULL, 2, NULL, NULL),
	(379, 109, 36, 0, NULL, 2, NULL, NULL),
	(380, 96, 36, 0, NULL, 2, NULL, NULL),
	(381, 136, 36, NULL, NULL, 1, '', NULL),
	(382, 137, 36, NULL, NULL, 1, '', NULL),
	(383, 134, 36, 2, 1, 1, '383', NULL),
	(384, 135, 36, NULL, NULL, 1, '', NULL),
	(385, 132, 36, NULL, NULL, 1, '', NULL),
	(386, 133, 36, NULL, NULL, 1, '', NULL),
	(387, 130, 36, NULL, NULL, 1, '', NULL),
	(388, 131, 36, NULL, NULL, 1, '', NULL),
	(389, 129, 36, NULL, NULL, 1, '', NULL),
	(390, 138, 36, NULL, NULL, 1, '', NULL),
	(391, 127, 37, 2, 1, 1, '370', NULL),
	(392, 126, 37, 0, 1, 1, '367', NULL),
	(393, 117, 37, 0, 1, 1, '334', NULL),
	(394, 119, 37, 0, 1, 1, '341', NULL),
	(395, 121, 37, 0, 1, 1, '349', NULL),
	(396, 120, 37, 0, 1, 1, '345', NULL),
	(397, 123, 37, 0, 1, 1, '357', NULL),
	(398, 122, 37, 0, 1, 1, '352', NULL),
	(399, 125, 37, 2, 1, 1, '366', NULL),
	(400, 124, 37, 0, 1, 1, '360', NULL),
	(401, 112, 37, 0, 1, 2, '[312,313,314]', NULL),
	(402, 101, 37, 0, 1, 2, '[267,268]', NULL),
	(403, 102, 37, 0, 1, 2, '[271,272]', NULL),
	(404, 100, 37, 2, 1, 2, '[263]', NULL),
	(405, 111, 37, 2, 1, 2, '[308,311,310,309]', NULL),
	(406, 105, 37, 2, 1, 2, '[283,284,285,286]', NULL),
	(407, 114, 37, 2, 1, 2, '[321,323,322]', NULL),
	(408, 104, 37, 0, 1, 2, '[279,280,282,281]', NULL),
	(409, 108, 37, 0, 1, 2, '[296]', NULL),
	(410, 94, 37, 0, 1, 2, '[235,237]', NULL),
	(411, 137, 37, 0, 1, 4, '390', NULL),
	(412, 134, 37, 0, 1, 4, '384', NULL),
	(413, 135, 37, 2, 1, 4, '385', NULL),
	(414, 132, 37, 2, 1, 4, '379', NULL),
	(415, 133, 37, 2, 1, 4, '381', NULL),
	(416, 130, 37, 2, 1, 4, '375', NULL),
	(417, 131, 37, 2, 1, 4, '377', NULL),
	(418, 129, 37, 2, 1, 4, '373', NULL),
	(419, 138, 37, 0, 1, 4, '392', NULL),
	(420, 139, 37, 0, 1, 4, '394', NULL),
	(421, 116, 38, NULL, NULL, 1, 'false', NULL),
	(422, 126, 38, NULL, NULL, 1, 'false', NULL),
	(423, 117, 38, NULL, NULL, 1, 'false', NULL),
	(424, 119, 38, NULL, NULL, 1, 'false', NULL),
	(425, 121, 38, NULL, NULL, 1, 'false', NULL),
	(426, 120, 38, NULL, NULL, 1, 'false', NULL),
	(427, 123, 38, NULL, NULL, 1, 'false', NULL),
	(428, 122, 38, NULL, NULL, 1, 'false', NULL),
	(429, 125, 38, NULL, NULL, 1, 'false', NULL),
	(430, 124, 38, NULL, NULL, 1, 'false', NULL),
	(431, 101, 38, 0, NULL, 2, NULL, NULL),
	(432, 102, 38, 0, NULL, 2, NULL, NULL),
	(433, 113, 38, 0, NULL, 2, NULL, NULL),
	(434, 110, 38, 0, NULL, 2, NULL, NULL),
	(435, 100, 38, 0, NULL, 2, NULL, NULL),
	(436, 99, 38, 0, NULL, 2, NULL, NULL),
	(437, 97, 38, 0, NULL, 2, NULL, NULL),
	(438, 107, 38, 0, NULL, 2, NULL, NULL),
	(439, 96, 38, 0, NULL, 2, NULL, NULL),
	(440, 108, 38, 0, NULL, 2, NULL, NULL),
	(441, 136, 38, NULL, NULL, 4, 'false', NULL),
	(442, 137, 38, NULL, NULL, 4, 'false', NULL),
	(443, 132, 38, NULL, NULL, 4, 'false', NULL),
	(444, 133, 38, NULL, NULL, 4, 'false', NULL),
	(445, 130, 38, NULL, NULL, 4, 'false', NULL),
	(446, 131, 38, NULL, NULL, 4, 'false', NULL),
	(447, 129, 38, NULL, NULL, 4, 'false', NULL),
	(448, 138, 38, NULL, NULL, 4, 'false', NULL),
	(449, 128, 38, NULL, NULL, 4, 'false', NULL),
	(450, 139, 38, NULL, NULL, 4, 'false', NULL),
	(451, 116, 39, NULL, NULL, 1, 'false', NULL),
	(452, 127, 39, NULL, NULL, 1, 'false', NULL),
	(453, 126, 39, NULL, NULL, 1, 'false', NULL),
	(454, 118, 39, NULL, NULL, 1, 'false', NULL),
	(455, 117, 39, NULL, NULL, 1, 'false', NULL),
	(456, 119, 39, NULL, NULL, 1, 'false', NULL),
	(457, 121, 39, NULL, NULL, 1, 'false', NULL),
	(458, 120, 39, NULL, NULL, 1, 'false', NULL),
	(459, 125, 39, NULL, NULL, 1, 'false', NULL),
	(460, 124, 39, NULL, NULL, 1, 'false', NULL),
	(461, 101, 39, 0, NULL, 2, NULL, NULL),
	(462, 110, 39, 0, NULL, 2, NULL, NULL),
	(463, 111, 39, 0, NULL, 2, NULL, NULL),
	(464, 105, 39, 0, NULL, 2, NULL, NULL),
	(465, 106, 39, 0, NULL, 2, NULL, NULL),
	(466, 103, 39, 0, NULL, 2, NULL, NULL),
	(467, 97, 39, 0, NULL, 2, NULL, NULL),
	(468, 107, 39, 0, NULL, 2, NULL, NULL),
	(469, 96, 39, 0, NULL, 2, NULL, NULL),
	(470, 93, 39, 0, NULL, 2, NULL, NULL),
	(471, 136, 39, NULL, NULL, 4, 'false', NULL),
	(472, 134, 39, NULL, NULL, 4, 'false', NULL),
	(473, 135, 39, NULL, NULL, 4, 'false', NULL),
	(474, 132, 39, NULL, NULL, 4, 'false', NULL),
	(475, 133, 39, NULL, NULL, 4, 'false', NULL),
	(476, 130, 39, NULL, NULL, 4, 'false', NULL),
	(477, 131, 39, NULL, NULL, 4, 'false', NULL),
	(478, 129, 39, NULL, NULL, 4, 'false', NULL),
	(479, 128, 39, NULL, NULL, 4, 'false', NULL),
	(480, 139, 39, NULL, NULL, 4, 'false', NULL),
	(481, 116, 40, NULL, NULL, 1, 'false', NULL),
	(482, 127, 40, NULL, NULL, 1, 'false', NULL),
	(483, 126, 40, NULL, NULL, 1, 'false', NULL),
	(484, 118, 40, NULL, NULL, 1, 'false', NULL),
	(485, 117, 40, NULL, NULL, 1, 'false', NULL),
	(486, 121, 40, NULL, NULL, 1, 'false', NULL),
	(487, 120, 40, NULL, NULL, 1, 'false', NULL),
	(488, 123, 40, NULL, NULL, 1, 'false', NULL),
	(489, 122, 40, NULL, NULL, 1, 'false', NULL),
	(490, 125, 40, NULL, NULL, 1, 'false', NULL),
	(491, 101, 40, 0, NULL, 2, NULL, NULL),
	(492, 113, 40, 0, NULL, 2, NULL, NULL),
	(493, 110, 40, 0, NULL, 2, NULL, NULL),
	(494, 106, 40, 0, NULL, 2, NULL, NULL),
	(495, 99, 40, 0, NULL, 2, NULL, NULL),
	(496, 109, 40, 0, NULL, 2, NULL, NULL),
	(497, 97, 40, 0, NULL, 2, NULL, NULL),
	(498, 107, 40, 0, NULL, 2, NULL, NULL),
	(499, 96, 40, 0, NULL, 2, NULL, NULL),
	(500, 93, 40, 0, NULL, 2, NULL, NULL),
	(501, 136, 40, NULL, NULL, 4, 'false', NULL),
	(502, 137, 40, NULL, NULL, 4, 'false', NULL),
	(503, 135, 40, NULL, NULL, 4, 'false', NULL),
	(504, 132, 40, NULL, NULL, 4, 'false', NULL),
	(505, 133, 40, NULL, NULL, 4, 'false', NULL),
	(506, 130, 40, NULL, NULL, 4, 'false', NULL),
	(507, 131, 40, NULL, NULL, 4, 'false', NULL),
	(508, 129, 40, NULL, NULL, 4, 'false', NULL),
	(509, 138, 40, NULL, NULL, 4, 'false', NULL),
	(510, 139, 40, NULL, NULL, 4, 'false', NULL),
	(511, 127, 41, NULL, NULL, 1, 'false', NULL),
	(512, 116, 41, NULL, NULL, 1, 'false', NULL),
	(513, 118, 41, NULL, NULL, 1, 'false', NULL),
	(514, 117, 41, NULL, NULL, 1, 'false', NULL),
	(515, 119, 41, NULL, NULL, 1, 'false', NULL),
	(516, 121, 41, NULL, NULL, 1, 'false', NULL),
	(517, 120, 41, NULL, NULL, 1, 'false', NULL),
	(518, 123, 41, NULL, NULL, 1, 'false', NULL),
	(519, 122, 41, NULL, NULL, 1, 'false', NULL),
	(520, 124, 41, NULL, NULL, 1, 'false', NULL),
	(521, 112, 41, 0, NULL, 2, NULL, NULL),
	(522, 102, 41, 0, NULL, 2, NULL, NULL),
	(523, 110, 41, 0, NULL, 2, NULL, NULL),
	(524, 100, 41, 0, NULL, 2, NULL, NULL),
	(525, 103, 41, 0, NULL, 2, NULL, NULL),
	(526, 97, 41, 0, NULL, 2, NULL, NULL),
	(527, 98, 41, 0, NULL, 2, NULL, NULL),
	(528, 96, 41, 0, NULL, 2, NULL, NULL),
	(529, 108, 41, 0, NULL, 2, NULL, NULL),
	(530, 94, 41, 0, NULL, 2, NULL, NULL),
	(531, 136, 41, NULL, NULL, 4, 'false', NULL),
	(532, 137, 41, NULL, NULL, 4, 'false', NULL),
	(533, 135, 41, NULL, NULL, 4, 'false', NULL),
	(534, 132, 41, NULL, NULL, 4, 'false', NULL),
	(535, 133, 41, NULL, NULL, 4, 'false', NULL),
	(536, 130, 41, NULL, NULL, 4, 'false', NULL),
	(537, 131, 41, NULL, NULL, 4, 'false', NULL),
	(538, 129, 41, NULL, NULL, 4, 'false', NULL),
	(539, 138, 41, NULL, NULL, 4, 'false', NULL),
	(540, 128, 41, NULL, NULL, 4, 'false', NULL),
	(541, 116, 43, 2, 1, 1, '330', NULL),
	(542, 127, 43, 0, 1, 1, '369', NULL),
	(543, 126, 43, 2, 1, 1, '368', NULL),
	(544, 118, 43, 0, 1, 1, '338', NULL),
	(545, 121, 43, 0, 1, 1, '350', NULL),
	(546, 120, 43, 0, 1, 1, '344', NULL),
	(547, 123, 43, 0, 1, 1, '356', NULL),
	(548, 122, 43, 0, 1, 1, '353', NULL),
	(549, 125, 43, 0, 1, 1, '365', NULL),
	(550, 124, 43, 0, 1, 1, '359', NULL),
	(551, 101, 43, 0, 1, 2, '[267,268]', NULL),
	(552, 112, 43, 2, 1, 2, '[312,313]', NULL),
	(553, 111, 43, 0, 1, 2, '[310]', NULL),
	(554, 106, 43, 0, 1, 2, '[288,289]', NULL),
	(555, 115, 43, 0, 1, 2, '[324,325]', NULL),
	(556, 109, 43, 0, 1, 2, '[302,301]', NULL),
	(557, 98, 43, 0, 1, 2, '[253]', NULL),
	(558, 107, 43, 0, 1, 2, '[294]', NULL),
	(559, 108, 43, 0, 1, 2, '[298]', NULL),
	(560, 96, 43, 0, 1, 2, '[244]', NULL),
	(561, 136, 43, 2, 1, 4, '387', NULL),
	(562, 137, 43, 2, 1, 4, '389', NULL),
	(563, 134, 43, 2, 1, 4, '383', NULL),
	(564, 135, 43, 2, 1, 4, '385', NULL),
	(565, 132, 43, 2, 1, 4, '379', NULL),
	(566, 133, 43, 2, 1, 4, '381', NULL),
	(567, 130, 43, 2, 1, 4, '375', NULL),
	(568, 138, 43, 2, 1, 4, '391', NULL),
	(569, 128, 43, 2, 1, 4, '371', NULL),
	(570, 139, 43, 2, 1, 4, '393', NULL),
	(571, 127, 44, 2, 1, 1, '370', NULL),
	(572, 116, 44, NULL, 1, 1, '328', NULL),
	(573, 126, 44, NULL, 1, 1, '367', NULL),
	(574, 118, 44, NULL, 1, 1, '338', NULL),
	(575, 117, 44, NULL, 1, 1, '333', NULL),
	(576, 119, 44, 2, 1, 1, '342', NULL),
	(577, 121, 44, NULL, 1, 1, '350', NULL),
	(578, 123, 44, NULL, 1, 1, '357', NULL),
	(579, 125, 44, NULL, 1, 1, '365', NULL),
	(580, 124, 44, 2, 1, 1, '362', NULL),
	(581, 101, 44, 0, 1, 2, '[269,268,267]', NULL),
	(582, 110, 44, 2, 1, 2, '[306,305,307]', NULL),
	(583, 111, 44, 2, 1, 2, '[310,309,308,311]', NULL),
	(584, 100, 44, 0, 1, 2, '[263,264,265,266]', NULL),
	(585, 105, 44, 2, 1, 2, '[284,283,285,286]', NULL),
	(586, 104, 44, 0, 1, 2, '[280,279,282,281]', NULL),
	(587, 97, 44, 0, 1, 2, '[248,249,251,250,252]', NULL),
	(588, 95, 44, 2, 1, 2, '[239,238,241,240]', NULL),
	(589, 107, 44, 0, 1, 2, '[294,293,292,295]', NULL),
	(590, 108, 44, 0, 1, 2, '[298]', NULL),
	(591, 136, 44, NULL, 1, 4, '388', NULL),
	(592, 137, 44, 2, 1, 4, '389', NULL),
	(593, 134, 44, 2, 1, 4, '383', NULL),
	(594, 135, 44, NULL, 1, 4, '386', NULL),
	(595, 132, 44, NULL, 1, 4, '380', NULL),
	(596, 133, 44, 2, 1, 4, '381', NULL),
	(597, 130, 44, 2, 1, 4, '375', NULL),
	(598, 131, 44, 2, 1, 4, '377', NULL),
	(599, 138, 44, NULL, 1, 4, '392', NULL),
	(600, 139, 44, 2, 1, 4, '393', NULL),
	(601, 116, 45, NULL, NULL, 1, '', NULL),
	(602, 127, 45, NULL, NULL, 1, '', NULL),
	(603, 126, 45, NULL, NULL, 1, '', NULL),
	(604, 117, 45, NULL, NULL, 1, '', NULL),
	(605, 119, 45, NULL, NULL, 1, '', NULL),
	(606, 121, 45, NULL, NULL, 1, '', NULL),
	(607, 120, 45, NULL, NULL, 1, '', NULL),
	(608, 123, 45, NULL, NULL, 1, '', NULL),
	(609, 122, 45, NULL, NULL, 1, '', NULL),
	(610, 125, 45, NULL, NULL, 1, '', NULL),
	(611, 113, 45, 0, NULL, 2, NULL, NULL),
	(612, 100, 45, 0, NULL, 2, NULL, NULL),
	(613, 111, 45, 0, NULL, 2, NULL, NULL),
	(614, 106, 45, 0, NULL, 2, NULL, NULL),
	(615, 99, 45, 0, NULL, 2, NULL, NULL),
	(616, 115, 45, 0, NULL, 2, NULL, NULL),
	(617, 109, 45, 0, NULL, 2, NULL, NULL),
	(618, 107, 45, 0, NULL, 2, NULL, NULL),
	(619, 96, 45, 0, NULL, 2, NULL, NULL),
	(620, 93, 45, 0, NULL, 2, NULL, NULL),
	(621, 136, 45, NULL, NULL, 4, '', NULL),
	(622, 137, 45, NULL, NULL, 4, '', NULL),
	(623, 132, 45, NULL, NULL, 4, '', NULL),
	(624, 133, 45, NULL, NULL, 4, '', NULL),
	(625, 130, 45, NULL, NULL, 4, '', NULL),
	(626, 131, 45, NULL, NULL, 4, '', NULL),
	(627, 129, 45, NULL, NULL, 4, '', NULL),
	(628, 138, 45, NULL, NULL, 4, '', NULL),
	(629, 139, 45, NULL, NULL, 4, '', NULL),
	(630, 128, 45, NULL, NULL, 4, '', NULL);
/*!40000 ALTER TABLE `exam_upq` ENABLE KEYS */;

-- 正在导出表  openexam.exam_user 的数据：~1 rows (大约)
DELETE FROM `exam_user`;
/*!40000 ALTER TABLE `exam_user` DISABLE KEYS */;
INSERT INTO `exam_user` (`id`, `workno`, `name`, `departmentid`, `distinction`, `idnumber`, `phone`, `usertypeid`) VALUES
	(7, '61459', '黄俊中', NULL, NULL, NULL, NULL, NULL);
/*!40000 ALTER TABLE `exam_user` ENABLE KEYS */;

-- 正在导出表  openexam.exam_userpaper 的数据：~3 rows (大约)
DELETE FROM `exam_userpaper`;
/*!40000 ALTER TABLE `exam_userpaper` DISABLE KEYS */;
INSERT INTO `exam_userpaper` (`id`, `fraction`, `teacherid`, `finish`, `display`, `examid`, `subproid`, `submitdate`, `lefttime`, `workno`, `hasview`) VALUES
	(43, 26, NULL, 100, 0, 8, NULL, '2019-07-04 15:36:52', 3284994, '61459', 1),
	(44, 26, NULL, 100, 0, 8, NULL, '2019-07-04 16:41:25', 2919997, '61459', 1),
	(45, 0, NULL, 0, 0, 8, NULL, '2019-07-15 15:07:35', 999, '61459', 1);
/*!40000 ALTER TABLE `exam_userpaper` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
