import request from '@/utils/request';

export function DoLogin(query) {
	return request({
		url: '/examonline/doLogin',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
				'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function RegiSter(query) {
	return request({
		url: '/examonline/saveUser',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
				'data': encodeURI(JSON.stringify(query))
		}

	})
};