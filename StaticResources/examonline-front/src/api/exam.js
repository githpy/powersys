import request from '@/utils/request';

export function randomQuestion(query) {
	return request({
		url: '/examonline/randomQuestion',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded;charset=gb2312'

		},
		params: {
				'data': encodeURI(JSON.stringify(query))
		}

	})
};


export function HasExam(query) {
	return request({
		url: '/examonline/hasExam',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		params: {
				'data': encodeURI(JSON.stringify(query))
		}
		
	})
};

export function GetExamPaperInfo(query) {
	return request({
		url: '/examonline/getExamPaperInfo',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		params: {
				'data': encodeURI(JSON.stringify(query))
		}
		
	})
};

export function GetBackViewPaper(query) {
	return request({
		url: '/examonline/getBackViewPaper',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		params: {
				'data': encodeURI(JSON.stringify(query))
		}
		
	})
};




export function PageCXCJ(query) {
	return request({
		url: '/examonline/pageCXCJ',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		params: {
				'data': encodeURI(JSON.stringify(query))
		}
		
	})
};

export function GetKSCJ(query) {
	return request({
		url: '/examonline/getKSCJ',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		params: {
				'data': encodeURI(JSON.stringify(query))
		}
		
	})
};

export function SavePaper(query) {
	return request({
		url: '/examonline/savePaper',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		params: {
				'data': encodeURI(JSON.stringify(query))
		}
		
	})
};

export function DoResult(query) {
	return request({
		url: '/examonline/doResult',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
		},
		params: {
				'data': query
		}
		
	})
};