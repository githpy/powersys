import Cookies from 'js-cookie'

const TokenKey = 'examonline-front'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
	             removeToken();
	             removeCurrentToken();
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function removeCurrentToken(){
	return Cookies.remove(TokenKey, { path: '/' });
}
