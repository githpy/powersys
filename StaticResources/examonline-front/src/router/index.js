import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
import Layout from '@/views/layout'


export const HasAuthRouter = [{
	path: '/',
	component: Layout,
	redirect: '/dashboard',
	hidden: false,
	children: [{
		path: 'dashboard',
		name: 'Dashboard',
		component: () => import('@/views/dashboard/index'),
		meta: {
			title: 'Dashboard',
			icon: 'dashboard'
		}
	},{
		path: 'exam',
		name:'exam',
		component: () =>
			import('@/views/exam'),
		hidden: true
	},{
		path: 'cxksexam',
		name:'cxksexam',
		component: () =>
			import('@/views/exam/cxksexam'),
		hidden: true
	}]
},{
		path: '/paper',
		name:'paper',
		component: () =>
			import('@/views/exam/paper'),
		hidden: true
	},{
		path: '/result',
		name:'result',
		component: () =>
			import('@/views/exam/result'),
		hidden: true
	},{
		path: '/backview',
		name:'backview',
		component: () =>
			import('@/views/exam/backview'),
		hidden: true
	}];

export const NoAuthRouter=[{
		path: '/404',
		component: () =>
			import('@/views/noauthviews/404'),
		hidden: true
	}];
const  aRouter=HasAuthRouter.concat(NoAuthRouter);

export default new Router({
	routes: aRouter
})
