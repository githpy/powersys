// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import Viser from 'viser-vue'
import router from './router'
import utils from './common/js/utils'
import store from './store'
import './permission' // permission control
Vue.config.productionTip = false;
Vue.prototype.utils=utils;
Vue.use(Viser)
Vue.use(Antd)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,store,
  components: { App },
  template: '<App/>'
})
