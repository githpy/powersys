import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)
import MenuView from '@/layouts/MenuView'
import RouteView from '@/layouts/RouteView'
export const HasAuthRouter = [{
	path: '/',
	component: MenuView,
	icon: 'none',
	redirect: '/index',
	invisible: true,
	children: [{
		path: '/index',
		name: '首页',
		component: RouteView,
		icon: 'dashboard',
		component: () =>
			import('@/views/index')

	}, {
		path: '/exam',
		name: '在线考试',
		component: RouteView,
		icon: 'dashboard',
		children: [{
			path: '/exam/ksgl',
			name: '考生管理',
			component: () =>
				import('@/views/exam/ksgl'),
			icon: 'none'
		},{
			path: '/exam/kszs',
			name: '考试设置',
			component: () =>
				import('@/views/exam/kszs'),
			icon: 'none'
		},{
			path: '/exam/sjgl',
			name: '试卷管理',
			component: () =>
				import('@/views/exam/sjgl'),
			icon: 'none'
		} ,{
			path: '/exam/stgl',
			name: '试题管理',
			component: () =>
				import('@/views/exam/stgl'),
			icon: 'none'
		} ,{
			path: '/exam/dataexport',
			name: '数据导出',
			component: () =>
				import('@/views/exam/dataexport'),
			icon: 'none'
		}]
	}]
}];

export const NoAuthRouter = [{
	path: '/login',
	component: () =>
		import('@/views/login'),
	hidden: true
},{
	path: '/404',
	component: () =>
		import('@/views/noauthviews/404'),
	hidden: true
}];
const aRouter = HasAuthRouter.concat(NoAuthRouter);

export default new Router({
	routes: aRouter
})
