import request from '@/utils/request';


export function AdminDoLogin(query) {
	return request({
		url: '/examonline/admin/doLogin',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};


export function DataExport(query) {
	return request({
		url: '/examonline/admin/dataExport',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};


export function DownModelExcelFile(query) {
	return request({
		url: '/examonline/admin/downModelExcelFile',
		method: 'get'
	})
};

export function GetUserPapers(query) {
	return request({
		url: '/examonline/admin/getUserPapers',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function GetUsers(query) {
	return request({
		url: '/examonline/admin/getUsers',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function UpdateUserBymodel(query) {
	return request({
		url: '/examonline/admin/updateUserBymodel',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function SaveUserBymodel(query) {
	return request({
		url: '/examonline/admin/saveUserBymodel',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};


export function GetUqpByMarking(query) {
	return request({
		url: '/examonline/admin/getUqpByMarking',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function SaveMarking(query) {
	return request({
		url: '/examonline/admin/saveMarking',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};



export function GeteqQuestions(query) {
	return request({
		url: '/examonline/admin/getQuestions',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};



export function EditSubPro(query) {
	return request({
		url: '/examonline/admin/editSubPro',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};


export function SaveQuestionByExcel(query) {
	return request({
		url: '/examonline/admin/saveQuestionByExcel',
		method: 'post',
		headers: {
			'Content-Type': 'application/json'
		
		},
		data: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function EditResult(query) {
	return request({
		url: '/examonline/admin/editResult',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};


export function GetExam(query) {
	return request({
		url: '/examonline/admin/getExam',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function ActionExam(query) {
	return request({
		url: '/examonline/admin/actionExam',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function UpdateQuestion(query) {
	return request({
		url: '/examonline/admin/updateQuestion',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};


export function GetEds(query) {
	return request({
		url: '/examonline/admin/preEdit',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};




export function GetSubPro(query) {
	return request({
		url: '/examonline/admin/getSubPro',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': query
		}

	})
};

export function getMarking(query) {
	return request({
		url: '/gqict/exam/getMarking',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function saveQuestion(query) {
	return request({
		url: '/gqict/exam/saveQuestion',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};

export function saveQuestionssByExcelsssss(query) {
	return request({
		url: '/gqict/exam/saveQuestionByExcel',
		method: 'post',
		data: JSON.stringify(query)

	})
};

export function saveQuestionByExcel(query) {
	return request({
		url: '/gqict/exam/saveQuestionByExcel',
		method: 'post',
		params: {
			'data': encodeURI(JSON.stringify(query))
		}
	})

}


export function saveExamPaper(query) {
	return request({
		url: '/gqict/exam/saveExam',
		method: 'post',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
};



export function GetResultByQuestionId(query) {
	return request({
		url: '/examonline/admin/getResultByQuestionId',
		method: 'get',
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded'

		},
		params: {
			'data': encodeURI(JSON.stringify(query))
		}

	})
}
