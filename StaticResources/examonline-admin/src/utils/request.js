import axios from 'axios'
import store from '../store'
import { getToken } from '@/utils/auth'
import  Message  from 'ant-design-vue/lib/message';
import 'ant-design-vue/lib/message/style/css'; 
// 创建axios实例
const service = axios.create({
  baseURL: process.env.BASE_API, // api 的 base_url
  timeout: 5000 // 请求超时时间
})

// request拦截器
 service.interceptors.request.use(
  config => {
      if(getToken()){
      config.headers['X-Token'] = JSON.parse(getToken()).workno // 让每个请求携带自定义token 请根据实际情况自行修改
    }
    return config
  },
  error => {
    // Do something with request error
    console.log(error) // for debug
    Promise.reject(error)
  }
) 

// response 拦截器
service.interceptors.response.use(
  response => {
     return response.data
  },
  error => {
    console.log('err' + error) // for debug
	Message.error('网络错误！')
    return Promise.reject(error)
  }
)

export default service
