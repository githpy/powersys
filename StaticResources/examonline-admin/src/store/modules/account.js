
const state={
		user: {}
};
const mutations={
		setuser(state, user) {
			state.user = user
		}
};
const actions={
		login({
			commit
		},user) {
			commit('setuser',user)
		}
	
};

export default {
	namespaced: true,
	state,
	mutations,
	actions
}
