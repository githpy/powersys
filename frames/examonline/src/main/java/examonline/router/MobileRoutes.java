package examonline.router;

import com.jfinal.config.Routes;

import examonline.controller.mobile.IndexController;


public class MobileRoutes extends Routes {
	public void config() {
		setBaseViewPath("/view/mobile");
		add("/mobile", IndexController.class);
	}

}
