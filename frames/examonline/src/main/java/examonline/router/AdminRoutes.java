package examonline.router;

import com.jfinal.config.Routes;

import examonline.controller.admin.IndexController;


public class AdminRoutes extends Routes {
	public void config() {
		setBaseViewPath("/view/admin");
		add("/admin", IndexController.class);
	}
}
