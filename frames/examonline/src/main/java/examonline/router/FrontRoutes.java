package examonline.router;
import com.jfinal.config.Routes;

import examonline.controller.front.IndexController;
public class FrontRoutes extends Routes {
	public void config() {
		setBaseViewPath("/view/front");
		add("/", IndexController.class);
	}
}
