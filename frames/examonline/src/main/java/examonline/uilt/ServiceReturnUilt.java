package examonline.uilt;

import java.util.HashMap;
import java.util.Map;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
@SuppressWarnings({  "rawtypes" })
public class ServiceReturnUilt {
	private static Map<String, Object> map;
    
	public static Map<String, Object> ServiceReturn(boolean result) {
		map = new HashMap<String, Object>();
		map.put("result", result);
		if (result == false)
			map.put("error", "");
		else
			map.put("error", "");
		return map;
	}

	public static Map<String, Object> ServiceReturnError(String error) {
		map = new HashMap<String, Object>();
		map.put("result", false);
			map.put("data", error);
			map.put("error", error);
		return map;
	}
	
	public static Map<String, Object> ServiceReturnRecordPage(Page page) {
		map = new HashMap<String, Object>();
		map.put("result", true);
		map.put("error", "");
		map.put("data", page.getList());
		map.put("recordsTotal", page.getTotalRow());
		map.put("recordsFiltered", page.getTotalRow());
		return map;
	}


	public static Map<String, Object> ServiceReturn(boolean result, Model model) {
		map = new HashMap<String, Object>();
		map.put("result", result);
		if (result == false)
			map.put("error", "");
		else
			map.put("error", "");
		map.put("data", model);
		return map;
	}

	public static Map<String, Object> ServiceReturn(boolean result, Object model) {
		map = new HashMap<String, Object>();
		map.put("result", result);
		if (result == false)
			map.put("error", "");
		else
			map.put("error", "");
		map.put("data", model);
		return map;
	}
}
