package examonline.uilt;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;


public class FastJsonUilt {
	// 把JSON文本parse为JSONObject或者JSONArray
	public static Object parse(String text) {
		return text;
	};

	// 把JSON文本parse成JSONObject
	public static JSONObject parseObject(String text) {
		return null;
	};

	// 把JSON文本parse为JavaBean
	public static <T> T parseObject(String text, Class<T> clazz) {
		return null;
	};

	// 把JSON文本parse成JSONArray
	public static JSONArray parseArray(String text) {
		return null;
	};

	// 把JSON文本parse成JavaBean集合
	public static <T> List<T> parseArray(String text, Class<T> clazz) {
		return JSONArray.parseArray(text, clazz);
	};

	// 将JavaBean序列化为JSON文本
	public static String toJSONString(Object object) {
		return null;
	};

	// 将JavaBean序列化为带格式的JSON文本
	public static String toJSONString(Object object, boolean prettyFormat) {
		return null;
	};

	// 将JavaBean转换为JSONObject或者JSONArray。
	public static Object toJSON(Object javaObject) {
		return javaObject;
	};

	public static Map<String, Object> convertJsonStrToMap(String jsonStr) {
		Map<String, Object> map = JSON.parseObject(jsonStr,
				new TypeReference<Map<String, Object>>() {
				});

		return map;
	}
}
