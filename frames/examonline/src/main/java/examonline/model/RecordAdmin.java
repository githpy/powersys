package examonline.model;

import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;


public class RecordAdmin {

	public Page<Record> userPaperPage(Map map) {
		Integer pageNumber = (Integer) map.get("page_number");
		Integer pageSize = (Integer) map.get("page_size");
		String order_dir = (String) map.get("order_dir");
		String search_value = (String) map.get("search_value");
		String seop = (String) map.get("select_value");
		String sqlPara = "SELECT eup.*,ee.name as examname,ee.hascompletion,ee.title,ee.invalid,eu.name as username  "
				+ " FROM exam_userpaper eup LEFT JOIN exam_exam ee ON eup.examid=ee.id LEFT JOIN exam_user eu ON eup.workno=eu.workno where 1=1"
				+ " and eup.workno=eu.workno ";
		if (seop != null && !seop.equals("") && !seop.trim().equals("全部")) {
			sqlPara += " and  " + seop + " like '%" + search_value + "%'";

		}
		return Db.paginate(pageNumber, pageSize, new SqlPara().setSql(sqlPara));
	}

	public List<Record> getUqpByMarking(Map map) {
		String sql = "SELECT question.id as questionid, question.title as questiontitle,questiontype.goal as qustiontypegoal,userpaper.id as userpaperid,upq.id as upqid, upq.goal as upqgoal,upq.resultinput as upqresultinput "
				+ " FROM exam_userpaper as userpaper LEFT JOIN exam_exam as exam ON userpaper.examid=exam.id LEFT JOIN "
				+ " exam_user as user ON userpaper.userid=user.id LEFT JOIN exam_upq as upq ON userpaper.id=upq.userpaperid LEFT JOIN "
				+ " exam_question as question ON upq.questionid=question.id LEFT JOIN exam_questiontype as questiontype ON questiontype.id=question.typeid "
				+ " WHERE 1=1 AND user.workno='" + map.get("workno") + "'" + " AND exam.id='" + map.get("examid")
				+ "' AND question.typeid='3'";
		return Db.find(sql);
	}

}
