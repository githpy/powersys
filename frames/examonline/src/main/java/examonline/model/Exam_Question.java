package examonline.model;

import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
public class Exam_Question extends AbstractModel<Exam_Question> {
	private static final Exam_Question dao = new Exam_Question().dao();

	public Exam_Question() {
		setDao(dao);
	}

	@Override
	public Page<Exam_Question> page(Map map) {
		Integer pageNumber = (Integer) map.get("page_number");
		Integer pageSize = (Integer) map.get("page_size");
		String order_dir = (String) map.get("order_dir");
		String search_value = (String) map.get("search_value");
		String seop = (String) map.get("select_value");
		String select = "select id,title,typeid,subjectid  ";
		String sqlExceptSelect = " from  " + _getTable().getName() + " where 1=? ";
		if (seop != null && !seop.equals("") && !seop.trim().equals("全部")) {
			sqlExceptSelect += " and  " + seop + " like '%" + search_value + "%'";

		}
		sqlExceptSelect += " GROUP BY id,title,typeid,subjectid  ORDER BY id " + order_dir;
		return dao.paginate(pageNumber, pageSize, true, select, sqlExceptSelect, 1);
	}
	
	public Page<Record> pages(Map map) {
		Integer pageNumber = (Integer) map.get("page_number");
		Integer pageSize = (Integer) map.get("page_size");
		String order_dir = (String) map.get("order_dir");
		String search_value = (String) map.get("search_value");
		String seop = (String) map.get("select_value");
		String sqlPara="SELECT eq.* "
				+ "FROM exam_question eq  LEFT   JOIN "
				+ " exam_questiontype eqs ON eqs.id=eq.typeid LEFT JOIN "
				+ "exam_project ep ON eq.projectid=ep.id LEFT JOIN"
				+ " exam_subject es ON es.id=ep.subjectid where 1=1";
		if (seop != null && !seop.equals("") && !seop.trim().equals("全部")) {
			sqlPara += " and  eq." + seop + " like '%" + search_value + "%'";

		}
		return Db.paginate(pageNumber, pageSize, new SqlPara().setSql(sqlPara));
	}
}

