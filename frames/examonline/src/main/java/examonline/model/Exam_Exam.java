package examonline.model;

import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;
public class Exam_Exam  extends AbstractModel<Exam_Exam>  {

	private  static final Exam_Exam dao=new Exam_Exam().dao();
	
	public Exam_Exam() {
		setDao(dao);
	}
	@Override
	public Page<Exam_Exam> page(Map map) {
		Integer pageNumber = (Integer) map.get("page_number");
		Integer pageSize = (Integer) map.get("page_size");
		String order_dir = (String) map.get("order_dir");
		String search_value = (String) map.get("search_value");
		String seop = (String) map.get("select_value");
		String select = "select id,title,time,stime,author,hascompletion,etime,invalid,display,name  ";
		String sqlExceptSelect = " from  " + _getTable().getName() + " where 1=? ";
		if (seop != null && !seop.equals("") && !seop.trim().equals("全部")) {
			sqlExceptSelect += " and  " + seop + " like '%" + search_value + "%'";

		}
		sqlExceptSelect += " GROUP BY id,title,time,stime,author,hascompletion,etime,invalid,display,name   ORDER BY id " + order_dir;
		return dao.paginate(pageNumber, pageSize, true, select, sqlExceptSelect, 1);
	}
   public List<Exam_Exam>getExamNoFinishs(){
	   String sql="select id,title,time,stime,author,hascompletion,etime,invalid,display  "
			   +" from  " + _getTable().getName() + " where 1=? "+" and display='1' and  invalid='0' ";
	return dao.find(sql);
	   
   }
   public Page<Record> pages(Map map) {
		Integer pageNumber = (Integer) map.get("page_number");
		Integer pageSize = (Integer) map.get("page_size");
		String order_dir = (String) map.get("order_dir");
		String search_value = (String) map.get("search_value");
		String seop = (String) map.get("select_value");
		String sqlPara = "SELECT  DISTINCT exam.id, exam.*,project.name as proname,subject.name as subname FROM exam_exam as exam "
				+ "LEFT JOIN exam_userpaper as userpaper ON userpaper.examid=exam.id "
				+ " LEFT JOIN exam_project as project ON exam.projectid=project.id "
				+ "  LEFT JOIN exam_subject as subject ON subject.id=project.subjectid "
				+ " WHERE exam.display='1' ";
		if (seop != null && !seop.equals("") && !seop.trim().equals("全部")) {
			sqlPara += " and " + seop + " like '%" + search_value + "%'";

		}
		return Db.paginate(pageNumber, pageSize, new SqlPara().setSql(sqlPara));
	}
}

