package examonline.model;

import java.util.Map;

import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("serial")
public class Exam_User extends AbstractModel<Exam_User> {
private  static final Exam_User dao=new Exam_User().dao();
	
	public Exam_User() {
		setDao(dao);
	}
	@Override
	public Page<Exam_User> page(Map map) {
		Integer pageNumber = (Integer) map.get("page_number");
		Integer pageSize = (Integer) map.get("page_size");
		String order_dir = (String) map.get("order_dir");
		String search_value = (String) map.get("search_value");
		String seop = (String) map.get("select_value");
		String select = "select workno,name,departmentid,distinction,idnumber,phone,usertypeid   ";
		String sqlExceptSelect = " from  " + _getTable().getName() + " where 1=? ";
		if (seop != null && !seop.equals("") && !seop.trim().equals("全部")) {
			sqlExceptSelect += " and  " + seop + " like '%" + search_value + "%'";

		}
		sqlExceptSelect += " GROUP BY  workno,name,departmentid,distinction,idnumber,phone,usertypeid    ORDER BY workno " + order_dir;
		return dao.paginate(pageNumber, pageSize, true, select, sqlExceptSelect, 1);
	}

}
