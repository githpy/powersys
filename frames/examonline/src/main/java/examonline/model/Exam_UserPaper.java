package examonline.model;

import java.util.Map;

import com.jfinal.plugin.activerecord.Page;

public class Exam_UserPaper extends AbstractModel<Exam_UserPaper> {
	private static final Exam_UserPaper dao = new Exam_UserPaper().dao();

	public Exam_UserPaper() {
		setDao(dao);
	}

	@Override
	public Page<Exam_UserPaper> page(Map map) {
		Integer pageNumber = (Integer) map.get("page_number");
		Integer pageSize = (Integer) map.get("page_size");
		String order_dir = (String) map.get("order_dir");
		String search_value = (String) map.get("search_value");
		String seop = (String) map.get("select_value");
		String select = "select id,userid,fraction,teacherid  ";
		String sqlExceptSelect = " from  " + _getTable().getName() + " where 1=? ";
		if (seop != null && !seop.equals("") && !seop.trim().equals("全部")) {
			sqlExceptSelect += " and  " + seop + " like '%" + search_value + "%'";

		}
		sqlExceptSelect += " GROUP BY id,userid,fraction,teacherid  ORDER BY id " + order_dir;
		return dao.paginate(pageNumber, pageSize, true, select, sqlExceptSelect, 1);
	}

}

