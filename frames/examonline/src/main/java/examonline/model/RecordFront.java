package examonline.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.SqlPara;

public class RecordFront {
	private Exam_upq upq = new Exam_upq();
	private Exam_Question question = new Exam_Question();
	private Exam_Result eqResult = new Exam_Result();
	private Exam_UserPaper eqUserPaper = new Exam_UserPaper();
	private Exam_Exam eqExam=new Exam_Exam();
	private Exam_ExamDetails eqDetails=new Exam_ExamDetails();
	public Page<Record> userPaperPage(Map map) {
		Integer pageNumber = (Integer) map.get("page_number");
		Integer pageSize = (Integer) map.get("page_size");
		String order_dir = (String) map.get("order_dir");
		String search_value = (String) map.get("search_value");
		String seop = (String) map.get("select_value");
		String sqlPara = "SELECT eup.*,ee.id as examid,ee.name as examname,ee.hascompletion,ee.title,ee.invalid,eu.name as username  "
				+ " FROM exam_userpaper eup LEFT JOIN exam_exam ee ON eup.examid=ee.id LEFT JOIN exam_user eu ON eup.workno=eu.workno where 1=1"
				+ " and eup.workno=eu.workno and ee.id='" + map.get("examid") + "' and eu.workno='" + map.get("workno")
				+ "'";
		if (seop != null && !seop.equals("") && !seop.trim().equals("全部")) {
			sqlPara += " and  " + seop + " like '%" + search_value + "%'";

		}
		return Db.paginate(pageNumber, pageSize, new SqlPara().setSql(sqlPara));
	}

	public List<Record> getUqpByMarking(Map map) {
		String sql = "SELECT question.id as questionid, question.title as questiontitle,questiontype.goal as qustiontypegoal,userpaper.id as userpaperid,upq.id as upqid, upq.goal as upqgoal,upq.resultinput as upqresultinput "
				+ " FROM exam_userpaper as userpaper LEFT JOIN exam_exam as exam ON userpaper.examid=exam.id LEFT JOIN "
				+ " exam_user as user ON userpaper.userid=user.id LEFT JOIN exam_upq as upq ON userpaper.id=upq.userpaperid LEFT JOIN "
				+ " exam_question as question ON upq.questionid=question.id LEFT JOIN exam_questiontype as questiontype ON questiontype.id=question.typeid "
				+ " WHERE 1=1 AND user.workno='" + map.get("workno") + "'" + " AND exam.id='" + map.get("examid")
				+ "' AND question.typeid='3'";
		return Db.find(sql);
	}
    /**
               * 获取会看试卷的数据,此方法不在直接分类，分类由前端处理
     * @param map
     * @return
     */
	public Map backViewPaper(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		List<PaperInfo> listInfo = new ArrayList<PaperInfo>();
		boolean hasView=false;
		// 查看是否存在未完成的试卷
		Exam_UserPaper ep = eqUserPaper
				.findFirst("select * from exam_userpaper where   workno='"
						+ map.get("workno") + "' and examid='" + map.get("examid") + "'"+" and id='"+map.get("userpaperid")+"'");
		if (ep != null) {
			Exam_Exam ee = eqExam.findById(map.get("examid"));
			Exam_ExamDetails eed = eqDetails.findById(ee.get("examdetailsid"));
			List<Exam_upq> upqs = upq
					.find("select * from exam_upq where " + " userpaperid='" + map.get("userpaperid") + "'");
			for (Exam_upq upq : upqs) {
				int type = upq.getInt("questiontypeid");
				// 获取问题
				Exam_Question eq = question.findFirst("select * from exam_question where id='"
						+ upq.getStr("questionid") + "' and typeid='" + type + "'");
				List<Exam_Result> ers=null;
				if(eq!=null) {
					// 获取答案
					ers=eqResult
							.find("select * from exam_result where questionid='" + eq.get("id") + "'");
				}
				
				PaperInfo info = new PaperInfo();
				info.setQuestion(eq);
				info.setResults(ers);
				info.setUpq(upq);
				listInfo.add(info);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("list",listInfo);
			jsonObject.put("exam", ee);
			jsonObject.put("eed", eed);
			jsonObject.put("userpaper", ep);
			jsonObject.put("hasView", hasView);
			remap.put("result", true);
			remap.put("error", "");
			remap.put("data", jsonObject);
		}
		return remap;
	}

	public class PaperInfo {
		private Exam_Question question;
		private List<Exam_Result> results;
		private Exam_Questiontype eqt;
		private Exam_upq upq;

		public Exam_upq getUpq() {
			return upq;
		}

		public void setUpq(Exam_upq upq) {
			this.upq = upq;
		}

		public Exam_Question getQuestion() {
			return question;
		}

		public void setQuestion(Exam_Question question) {
			this.question = question;
		}

		public List<Exam_Result> getResults() {
			return results;
		}

		public void setResults(List<Exam_Result> results) {
			this.results = results;
		}

		public Exam_Questiontype getEqt() {
			return eqt;
		}

		public void setEqt(Exam_Questiontype eqt) {
			this.eqt = eqt;
		}

	}

}
