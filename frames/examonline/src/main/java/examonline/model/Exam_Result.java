package examonline.model;

import java.util.Map;

import com.jfinal.plugin.activerecord.Page;


public class Exam_Result extends AbstractModel<Exam_Result> {
	private static final Exam_Result dao = new Exam_Result().dao();

	public Exam_Result() {
		setDao(dao);
	}

	@Override
	public Page<Exam_Result> page(Map map) {
		Integer pageNumber = (Integer) map.get("page_number");
		Integer pageSize = (Integer) map.get("page_size");
		String order_dir = (String) map.get("order_dir");
		String search_value = (String) map.get("search_value");
		String seop = (String) map.get("select_value");
		String select = "select id,text,right,qustionid  ";
		String sqlExceptSelect = " from  " + _getTable().getName() + " where 1=? ";
		if (seop != null && !seop.equals("") && !seop.trim().equals("全部")) {
			sqlExceptSelect += " and  " + seop + " like '%" + search_value + "%'";

		}
		sqlExceptSelect += " GROUP BY id,text,rightr,qustionid  ORDER BY id " + order_dir;
		return dao.paginate(pageNumber, pageSize, true, select, sqlExceptSelect, 1);
	}
   public Exam_Result getByFirstNameValue(String name, Object value) {
		String sql = " select * from exam_result r where r." + name + "= '" + value + "'";
		return dao.findFirst(sql);
	}
}

