package examonline.model;


import java.util.List;
import java.util.Map;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

import examonline.interfaces.ModelInterface;

@SuppressWarnings({ "rawtypes"})
public abstract class AbstractModel<T extends AbstractModel<T>> extends Model<T> implements ModelInterface<T> {

	private static final long serialVersionUID = 8516531538141370804L;
	private T dao;

	public void setDao(T dao) {
		this.dao = dao;
	}
	@Override
	public List<T> getByAttrNameValue(String name, Object value) {
        
		String sql = " select * from " + _getTable().getName() + " r where r." + name + "= '" + value + "'";
		return dao.find(sql);
	}
	@Override
	public T getByFirstNameValue(String name, Object value) {
		String sql = " select * from " + _getTable().getName() + " r where r." + name + "= '" + value + "'";
		return dao.findFirst(sql);
	}
	@Override
	public T getByFirstNameAndUrl(String name, String url) {
		String sql = " select * from " + _getTable().getName() + " r where r.name= '" + name + "'" + "and r.url='" + url
				+ "'";
		return dao.findFirst(sql);
	}
	@Override
	public List<T> getAll() {
		return dao.find("select * from " + _getTable().getName() + " r");
	}
	@Override
	public List<T> getByAttrNameValueLike(String name, Object value) {
		String sql = " select * from " + _getTable().getName() + " r where r." + name + " like '" + value + "%'";
		return dao.find(sql);
	}
	@Override
	public Page<T> page(int pageNumber, int pageSize) {
		return dao.paginate(pageNumber, pageSize, "select u.* ", " from " + _getTable().getName() + " u ");
	}
	@Override
	public Page<T> page(int pageNumber, int pageSize, Map map) {
		return null;
	};
	@Override
	public abstract Page<T> page(Map map);
	
	

}

