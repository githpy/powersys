package examonline.config;

import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.template.Engine;

import examonline.router.AdminRoutes;
import examonline.router.FrontRoutes;
import examonline.router.MobileRoutes;


public class AppConfig extends JFinalConfig {
	private Routes routes;

	@Override
	public void configConstant(Constants me) {
		me.setDevMode(true);
		// 开启注释
		me.setInjectDependency(true);

	}

	@Override
	public void configRoute(Routes me) {
		me.add(new FrontRoutes()); // 前端路由
		me.add(new AdminRoutes()); // 后端路由
		me.add(new MobileRoutes()); // 后端路由
		this.routes = me;
	}

	
	  public static void main(String[] args) { // eclipse 下的启动方式
	  JFinal.start("src/main/webapp", 8080, "/examonline", 5);
	  
	  }
	 

	@Override
	public void configEngine(Engine me) {

	}

	@Override
	public void configPlugin(Plugins me) {
		PropKit.use("jdbc.properties");
		C3p0Plugin cp = new C3p0Plugin(PropKit.get("jdbc.url"), PropKit.get("jdbc.username"),
				PropKit.get("jdbc.password"), PropKit.get("jdbc.driver"));
		cp.setInitialPoolSize(3);
		cp.setMaxIdleTime(10);
		cp.setMinPoolSize(3);
		cp.setMaxIdleTime(6);
		me.add(cp);
		ActiveRecordPlugin arp = new ActiveRecordPlugin(cp);
		DbTableConfigAdmin.configPlugin(arp);
		arp.setDialect(new MysqlDialect());
		arp.setShowSql(true);
		me.add(arp);
	}

	@Override
	public void configInterceptor(Interceptors me) {

	}

	@Override
	public void configHandler(Handlers me) {

	}

}
