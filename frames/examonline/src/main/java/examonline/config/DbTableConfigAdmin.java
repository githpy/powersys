package examonline.config;

import com.jfinal.plugin.activerecord.ActiveRecordPlugin;

import examonline.model.Exam_Exam;
import examonline.model.Exam_ExamDetails;
import examonline.model.Exam_Project;
import examonline.model.Exam_Question;
import examonline.model.Exam_Questiontype;
import examonline.model.Exam_Result;
import examonline.model.Exam_Subject;
import examonline.model.Exam_SysUser;
import examonline.model.Exam_UserPaper;
import examonline.model.Exam_upq;
import examonline.model.Exam_User;

public class DbTableConfigAdmin {
	public static void configPlugin(ActiveRecordPlugin arp) {
		// 在线考试
		arp.addMapping("exam_question", Exam_Question.class);
		arp.addMapping("exam_userpaper", Exam_UserPaper.class);
		arp.addMapping("exam_result", Exam_Result.class);
		arp.addMapping("exam_exam", Exam_Exam.class);
		arp.addMapping("exam_upq", Exam_upq.class);
		arp.addMapping("exam_user", "workno",Exam_User.class);
		arp.addMapping("exam_questiontype", Exam_Questiontype.class);
		arp.addMapping("exam_project", Exam_Project.class);
		arp.addMapping("exam_subject", Exam_Subject.class);
		arp.addMapping("exam_examdetails", Exam_ExamDetails.class);
		arp.addMapping("exam_sysuser", Exam_SysUser.class);
		
	}
}
