package examonline.interfaces;

import java.util.ArrayList;
import java.util.Map;

import com.jfinal.plugin.activerecord.Model;
@SuppressWarnings("rawtypes")
public interface ServiceInterface {
	public Map getByAttrNameValue(String name, String value);

	public Map getByFirstNameValue(String name, String value);

	public Map getByFirstNameAndUrl(String name, String url);

	public Map getAll();

	public Map getByAttrNameValueLike(String name, String value);

	public Map page(int pageNumber, int pageSize);

	public Map page(int pageNumber, int pageSize, Map map);
	
	public Map page(Map map);

	public Map deletById(String id);
    
	public Map deletByIds(ArrayList<Integer>  ids);
	
	public Map add(Model model);

	public Map add(Map map);

	public Map update(Model model);

	public Map update(Map map);

	public Map delete(Map map);

	public Map delete(Model model);
}
