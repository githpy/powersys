package examonline.interfaces;

import java.util.List;
import java.util.Map;

import com.jfinal.plugin.activerecord.Page;

@SuppressWarnings("rawtypes")
public interface ModelInterface<T> {
	/**
	 * 根据表属性值查询
	 * 
	 * @param name
	 *            表字段
	 * @param value
	 *            表字段值
	 * @return 返回所有符合数据
	 */
	public List<T> getByAttrNameValue(String name, Object value);

	/**
	 * 根据表属性值查询
	 * 
	 * @param name
	 *            表字段
	 * @param value
	 *            表字段值
	 * @return 返回一条符合数据
	 */
	public T getByFirstNameValue(String name, Object value);

	/**
	 * 根据表Name字段的值和Url的值返回数据
	 * 
	 * @param name
	 *            Name字段值
	 * @param url
	 *            Url字段值
	 * @return 返回一条符合数据
	 */
	public T getByFirstNameAndUrl(String name, String url);

	/**
	 * 获取此表中所有数据
	 * 
	 * @return 所有数据
	 */
	public List<T> getAll();

	/**
	 * 根据字段名称，模糊搜索
	 * 
	 * @param name
	 *            字段名
	 * @param value
	 *            字段值
	 * @return 模糊查询数组结果
	 */
	public List<T> getByAttrNameValueLike(String name, Object value);

	/**
	 * 分页查询
	 * 
	 * @param pageNumber
	 *            当前页
	 * @param pageSize
	 *            页显示条数
	 * @return
	 */
	public Page<T> page(int pageNumber, int pageSize);

	/**
	 * 分页查询，业务需自定义
	 * 
	 * @param pageNumber
	 *            当前页
	 * @param pageSize
	 *            页显示条数
	 * @param map
	 *            自定义参数集合
	 * @return
	 */
	public Page<T> page(int pageNumber, int pageSize, Map map);

	public Page<T> page(Map map);

}

