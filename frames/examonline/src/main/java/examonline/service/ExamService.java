package examonline.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.aop.Before;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.tx.Tx;

import examonline.model.Exam_Exam;
import examonline.model.Exam_ExamDetails;
import examonline.model.Exam_Project;
import examonline.model.Exam_Question;
import examonline.model.Exam_Questiontype;
import examonline.model.Exam_Result;
import examonline.model.Exam_Subject;
import examonline.model.Exam_User;
import examonline.model.Exam_UserPaper;
import examonline.model.Exam_upq;
import examonline.model.RecordFront;
import examonline.uilt.DateUtil;
import examonline.uilt.FastJsonUilt;
import examonline.uilt.ServiceReturnUilt;

@SuppressWarnings({ "unchecked", "rawtypes" })
public class ExamService {
	public Exam_Question eqQuestion = new Exam_Question();
	public Exam_Result eqResult = new Exam_Result();
	public Exam_UserPaper eqUserPaper = new Exam_UserPaper();
	public Exam_Exam eqExam = new Exam_Exam();
	public Exam_upq eqUpq = new Exam_upq();
	public Exam_User eqUser = new Exam_User();
	public Exam_Questiontype eqQt = new Exam_Questiontype();
	public Exam_Project eqProject = new Exam_Project();
	public Exam_Subject subject = new Exam_Subject();
	public Exam_ExamDetails eqDetails = new Exam_ExamDetails();
    public Exam_Subject eqSubject=new Exam_Subject();
	public RecordFront recordFront = new RecordFront();
	public Map geteqUserPaperPage(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Page page = this.eqUserPaper.page(map);
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", page.getList());
		remap.put("recordsTotal", page.getTotalRow());
		remap.put("recordsFiltered", page.getTotalRow());
		return remap;
	}

	public Map geteqQuestion(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Page page = this.eqQuestion.page(map);
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", page.getList());
		remap.put("recordsTotal", page.getTotalRow());
		remap.put("recordsFiltered", page.getTotalRow());
		return remap;
	}

	public Map saveQuestion(Map map) {
		Map questiondata = (Map) map.get("questiondata");
		List<Map> resultdatas = (List<Map>) map.get("resultdatas");
		this.eqQuestion._setAttrs(questiondata);
		Map<String, Object> remap = new HashMap<String, Object>();
		this.eqQuestion.save();
		for (Map res : resultdatas) {
			res.put("questionid", this.eqQuestion.get("id"));
			this.eqResult.remove("id");
			this.eqResult._setAttrs(res);
			this.eqResult.save();
		}
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", "");
		return remap;
	}

	public Map saveExam(Map map) {
		this.eqExam._setAttrs(map);
		Map<String, Object> remap = new HashMap<String, Object>();
		boolean result = this.eqExam.save();
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", result);
		return remap;
	}

	public Map getExam(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Page page = this.eqExam.page(map);
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", page.getList());
		remap.put("recordsTotal", page.getTotalRow());
		remap.put("recordsFiltered", page.getTotalRow());
		return remap;
	}

	/**
	 * 获取考试
	 * 
	 * @return
	 */
	public Map getExamPaper() {
		Map<String, Object> remap = new HashMap<String, Object>();
		String sql = "select *,exam_userpaper.id as pid, exam_userpaper.display as pdisplay  from exam_userpaper LEFT JOIN exam_exam ON exam_userpaper.examid=exam_exam.id";
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", Db.find(sql));
		return remap;
	}

	/**
	 * 登陆
	 * 
	 * @param map
	 * @return
	 */
	public Map saveUser(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		String sql = "select * from exam_user where workno='" + map.get("workno") + "'";
		Exam_User user = eqUser.findFirst(sql);
		if (user != null) {
			remap.put("result", false);
			remap.put("error", "用户已存在");
			remap.put("data", "用户已存在");
		} else {
			eqUser.clear();
			eqUser._setAttrs(map);
			boolean b = eqUser.save();
			remap.put("result", b);
			remap.put("error", "");
			remap.put("data", b);
		}
		return remap;
	}

	public Map AuThUser(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		String sql = "select * from exam_user where workno='" + map.get("workno") + "' and name='" + map.get("name")
				+ "'";
		Exam_User user = eqUser.findFirst(sql);
		if (user == null) {
			remap.put("result", false);
			remap.put("error", "用户或密码错误");
			remap.put("data", "用户或密码错误");
		} else {
			remap.put("result", true);
			remap.put("error", "");
			remap.put("data", user);
		}
		return remap;
	}

	public Map getMarking(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Page page = this.eqExam.page(map);
		List<Exam_Exam> list = page.getList();
		List<ExamPapers> relist = new ArrayList<ExamPapers>();
		for (Exam_Exam ee : list) {
			ExamPapers ep = new ExamPapers();
			int examid = ee.get("id");
			ep.setExam(ee);
			ep.setList(eqUserPaper.getByAttrNameValue("examid", String.valueOf(examid)));
			relist.add(ep);
		}
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", relist);
		remap.put("recordsTotal", page.getTotalRow());
		remap.put("recordsFiltered", page.getTotalRow());
		return remap;
	}

	// 获取试卷对应的题目
	public Map getQuestions(String ExamPaperId) {
		Map<String, Object> remap = new HashMap<String, Object>();
		List<Exam_upq> qpqlist = eqUpq.getByAttrNameValue("userpaperid", ExamPaperId);
		List<PaperQuestionsResult> relist = new ArrayList<PaperQuestionsResult>();
		for (Exam_upq upq : qpqlist) {
			PaperQuestionsResult pqr = new PaperQuestionsResult();
			// 获取题目
			Exam_Question eq = eqQuestion.getByFirstNameValue("id", upq.get("questionid").toString());
			// 获取题目答案选项
			List<Exam_Result> erl = eqResult.getByAttrNameValue("questionid", eq.getStr("id"));
			pqr.setQuestion(eq);
			pqr.setResults(erl);
			relist.add(pqr);
		}
		Paper paper = new Paper();
		paper.setUpqs(qpqlist);
		paper.setPqrs(relist);
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", paper);
		return remap;
	}

	public class ExamPapers {
		private Exam_Exam exam;
		private List<Exam_UserPaper> list;

		public Exam_Exam getExam() {
			return exam;
		}

		public void setExam(Exam_Exam exam) {
			this.exam = exam;
		}

		public List<Exam_UserPaper> getList() {
			return list;
		}

		public void setList(List<Exam_UserPaper> list) {
			this.list = list;
		}

	}

	public class Paper {
		private List<PaperQuestionsResult> pqrs;
		private List<Exam_upq> upqs;

		public List<PaperQuestionsResult> getPqrs() {
			return pqrs;
		}

		public void setPqrs(List<PaperQuestionsResult> pqrs) {
			this.pqrs = pqrs;
		}

		public List<Exam_upq> getUpqs() {
			return upqs;
		}

		public void setUpqs(List<Exam_upq> upqs) {
			this.upqs = upqs;
		}

	}

	public class PaperQuestionsResult {
		private Exam_Question question;
		private List<Exam_Result> results;
		private Exam_Questiontype eqt;
		private Exam_ExamDetails eed;
		private Exam_upq upq;
		
		public Exam_upq getUpq() {
			return upq;
		}

		public void setUpq(Exam_upq upq) {
			this.upq = upq;
		}

		public Exam_ExamDetails getEed() {
			return eed;
		}

		public void setEed(Exam_ExamDetails eed) {
			this.eed = eed;
		}

		public Exam_Question getQuestion() {
			return question;
		}

		public void setQuestion(Exam_Question question) {
			this.question = question;
		}

		public List<Exam_Result> getResults() {
			return results;
		}

		public void setResults(List<Exam_Result> results) {
			this.results = results;
		}

		public Exam_Questiontype getEqt() {
			return eqt;
		}

		public void setEqt(Exam_Questiontype eqt) {
			this.eqt = eqt;
		}

	}

	public class PaperQuestionsResultMobile extends PaperQuestionsResult {
		private String answer = "";
		private String userAnswer = "";
		private boolean userFavor;
		private String explain;
        private Exam_upq upq;
        
        
		public Exam_upq getUpq() {
			return upq;
		}

		public void setUpq(Exam_upq upq) {
			this.upq = upq;
		}

		public String getAnswer() {
			return answer;
		}

		public void setAnswer(String answer) {
			this.answer = answer;
		}

		public String getUserAnswer() {
			return userAnswer;
		}

		public void setUserAnswer(String userAnswer) {
			this.userAnswer = userAnswer;
		}

		public boolean isUserFavor() {
			return userFavor;
		}

		public void setUserFavor(boolean userFavor) {
			this.userFavor = userFavor;
		}

		public String getExplain() {
			return explain;
		}

		public void setExplain(String explain) {
			this.explain = explain;
		}
	}

	@Before(Tx.class)
	public Map saveQuestionByExcel(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		boolean succeed = Db.tx(new IAtom() {
			public boolean run() throws SQLException {
				// 单选
				List dxquestions = (List) map.get("dxquestions");
				for (int i = 0; i < dxquestions.size(); i++) {
					// 读取问题
					JSONObject question = (JSONObject) dxquestions.get(i);
					Map qmap = FastJsonUilt.convertJsonStrToMap(question.toString());
					// 获取问题答案
					List results = (List) qmap.get("results");
					// 问题中移除答案
					qmap.remove("results"); // 存入问题
					eqQuestion.clear();
					eqQuestion._setAttrs(qmap);
					eqQuestion.save(); // 遍历答案
					for (int j = 0; j < results.size(); j++) {
						JSONObject result = (JSONObject) results.get(j);
						Map mresult = FastJsonUilt.convertJsonStrToMap(result.toString());
						mresult.put("questionid", eqQuestion.get("id")); // 存入答案 eqResult.clear();
																			// eqResult._setAttrs(mresult);
						eqResult.save();
					}

				} // 多选
				List dxxquestions = (List) map.get("dxxquestions");
				for (int i = 0; i < dxxquestions.size(); i++) {
					// 读取问题
					JSONObject question = (JSONObject) dxxquestions.get(i);
					Map qmap = FastJsonUilt.convertJsonStrToMap(question.toString());
					// 获取问题答案
					List results = (List) qmap.get("results");
					// 问题中移除答案
					qmap.remove("results");
					// 存入问题
					eqQuestion.clear();
					eqQuestion._setAttrs(qmap);
					eqQuestion.save();
					// 遍历答案
					for (int j = 0; j < results.size(); j++) {
						JSONObject result = (JSONObject) results.get(j);
						Map mresult = FastJsonUilt.convertJsonStrToMap(result.toString());
						mresult.put("questionid", eqQuestion.get("id"));
						// 存入答案
						eqResult.clear();
						eqResult._setAttrs(mresult);
						eqResult.save();
					}
				}
				// 判断题
				List pdquestions = (List) map.get("pdquestions");
				for (int i = 0; i < pdquestions.size(); i++) {
					// 读取问题
					JSONObject question = (JSONObject) pdquestions.get(i);
					Map qmap = FastJsonUilt.convertJsonStrToMap(question.toString()); // 存入问题
					eqQuestion.clear();
					eqQuestion._setAttrs(qmap);
					eqQuestion.save();
				}
				// 填空题
				List tkquestions = (List) map.get("pdquestions");
				for (int i = 0; i < tkquestions.size(); i++) {
					// 读取问题
					JSONObject question = (JSONObject) tkquestions.get(i);
					Map qmap = FastJsonUilt.convertJsonStrToMap(question.toString()); // 存入问题
					eqQuestion.clear();
					eqQuestion._setAttrs(qmap);
					eqQuestion.save();
				}

				return true;
			}

		});
		remap.put("result", succeed);
		remap.put("error", "");
		remap.put("data", "");
		return remap;
	}

	public Map randomQuestionMobile(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Exam_UserPaper ep = eqUserPaper
				.findFirst("select * from exam_userpaper where  finish<='100' and display='1' and workno='"
						+ map.get("workno") + "' and examid='" + map.get("examid") + "'");
		Exam_Exam ee = eqExam.findById(map.get("examid"));
		Exam_ExamDetails eed = eqDetails.findById(ee.get("examdetailsid"));
		// ep==null获取新题目
		if (ep == null) {
			List<PaperQuestionsResultMobile> dx = typeQestionMobile("1", eed.getInt("dxsize"), ee.get("subproid"));
			List<PaperQuestionsResultMobile> dxx = typeQestionMobile("2", eed.getInt("dxxsize"), ee.get("subproid"));
			List<PaperQuestionsResultMobile> pd = typeQestionMobile("4", eed.getInt("pdsize"), ee.get("subproid"));
			List<PaperQuestionsResultMobile> tk = typeQestionMobile("3", eed.getInt("tksize"), ee.get("subproid"));
			JSONObject jsonObject = new JSONObject();
			dx.addAll(dxx);
			dx.addAll(pd);
			dx.addAll(tk);
			jsonObject.put("list", dx);
			jsonObject.put("upqs", null);
			jsonObject.put("exam", ee);
			jsonObject.put("userpaper", null);
			remap.put("result", true);
			remap.put("eed", eed);
			remap.put("error", "");
			remap.put("data", jsonObject);
		} else {
			List<PaperQuestionsResultMobile> question = new ArrayList<PaperQuestionsResultMobile>();
			List<Exam_upq> upqs = eqUpq.find("select * from exam_upq where userpaperid='" + ep.get("id") + "'");
			for (Exam_upq upq : upqs) {
				int type = upq.getInt("questiontypeid");
				Exam_Question eq = eqQuestion.findFirst("select * from exam_question where id='"
						+ upq.getStr("questionid") + "' and typeid='" + type + "'");
				String rSql = "select * from exam_result WHERE questionid='" + eq.getInt("id") + "'";
				Exam_Questiontype eqt = eqQt.findById(eq.get("typeid"));
				List<Exam_Result> ers = eqResult
						.find("select * from exam_result where questionid='" + eq.get("id") + "'");
				PaperQuestionsResultMobile pqr = new PaperQuestionsResultMobile();
				// 解析符合手机版答案
				if (type == 2) {
					// 多选
					if (upq.get("useranswer") != null) {
						String rs = upq.get("useranswer");
						System.out.println(rs);
						pqr.setUserAnswer(rs);
					}
				} else {
					pqr.setUserAnswer(upq.get("useranswer"));
				}
				pqr.setQuestion(eq);
				pqr.setResults(ers);
				pqr.setEqt(eqt);
				question.add(pqr);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("list", question);
			jsonObject.put("upqs", null);
			jsonObject.put("userpaper", ep);
			jsonObject.put("exam", ee);
			remap.put("eed", eed);
			remap.put("result", true);
			remap.put("error", "");
			remap.put("data", jsonObject);
		}
		return remap;
	}

	private List<PaperQuestionsResult> typeQestion(String type, int top, Object subproid) {
		List<PaperQuestionsResult> relist = new ArrayList<PaperQuestionsResult>();
		String sql = "SELECT * FROM  exam_question WHERE typeid='" + type + "' and subproid='" + subproid
				+ "' ORDER BY RAND() LIMIT " + top;
		//如果==3就是制定到方案级别
		if(subproid.toString().trim().length()==3) {
			sql = "SELECT * FROM  exam_question WHERE typeid='"
		            + type + "' and subproid like '" + subproid.toString().split("-")[0]
					+ "%' ORDER BY RAND() LIMIT " + top;
		}
		List<Exam_Question> lExam_Questions = eqQuestion.find(sql);
		for (Exam_Question eq : lExam_Questions) {
			PaperQuestionsResult pqr = new PaperQuestionsResult();
			String rSql = "select * from exam_result WHERE questionid='" + eq.getInt("id") + "'";
			Exam_Questiontype eqt = eqQt.findById(eq.get("typeid"));
			pqr.setQuestion(eq);
			pqr.setResults(eqResult.find(rSql));
			pqr.setEqt(eqt);
			relist.add(pqr);
		}
		return relist;
	}

	private List<PaperQuestionsResultMobile> typeQestionMobile(String type, int top, Object subproid) {
		List<PaperQuestionsResultMobile> relist = new ArrayList<PaperQuestionsResultMobile>();
		String sql = "SELECT * FROM  exam_question WHERE typeid='" + type + "' and subproid='" + subproid
				+ "' ORDER BY RAND() LIMIT " + top;
		//如果==3就是制定到方案级别
		if(subproid.toString().trim().length()==3) {
			sql = "SELECT * FROM  exam_question WHERE typeid='"
		            + type + "' and subproid like '" + subproid.toString().split("-")[0]
					+ "%' ORDER BY RAND() LIMIT " + top;
		}
		List<Exam_Question> lExam_Questions = eqQuestion.find(sql);
		for (Exam_Question eq : lExam_Questions) {
			PaperQuestionsResultMobile pqr = new PaperQuestionsResultMobile();
			String rSql = "select * from exam_result WHERE questionid='" + eq.getInt("id") + "'";
			Exam_Questiontype eqt = eqQt.findById(eq.get("typeid"));
			pqr.setQuestion(eq);
			pqr.setResults(eqResult.find(rSql));
			pqr.setEqt(eqt);
			relist.add(pqr);
		}
		return relist;
	}

	public Exam_UserPaper savePaperV2(Map map) {
		// 用户试卷的部分信息
		JSONObject userpaper = (JSONObject) map.get("userpaper");

		String detailsql = "SELECT examdetails.* FROM exam_exam as exam "
				+ " LEFT JOIN exam_examdetails as examdetails ON" + " examdetails.id=exam.examdetailsid WHERE exam.id='"
				+ userpaper.get("examid") + "'";
		// 获取题型分数
		Exam_ExamDetails ed = eqDetails.findFirst(detailsql);
		int dxgoal = ed.getInt("dxcustomscore");
		int dxxgoal = ed.getInt("dxxcustomscore");
		int tkgoal = ed.getInt("tkcustomscore");
		int pdgoal = ed.getInt("pdcustomscore");
		Exam_UserPaper ep = eqUserPaper.findFirst("select * from exam_userpaper where workno='"
				+ userpaper.get("workno") + "'and display='1' and examid='" + userpaper.get("examid") + "'");
		// 判断用户是否已经参加过考试
		if (ep == null) {
			// 先保存userpaper
			eqUserPaper.clear();
			Map usermap = FastJsonUilt.convertJsonStrToMap(userpaper.toString());
			usermap.put("submitdate", new Date());
			eqUserPaper._setAttrs(usermap);
			eqUserPaper.save();
			ep = eqUserPaper;
		} else {
			Map usermap = FastJsonUilt.convertJsonStrToMap(userpaper.toString());
			usermap.put("submitdate", new Date());
			ep._setAttrs(usermap);
			ep.update();
		}
		// 试卷分数
		int fraction = 0;
		// 单选
		JSONObject dxquestions = (JSONObject) map.get("dxquestions");
		Set<String> keyset = dxquestions.keySet();
		for (String key : keyset) {
			Exam_upq upq = eqUpq.findFirst("select * from exam_upq where questionid='" + strQint(key)
					+ "' and userpaperid='" + ep.getInt("id") + "'");
			int checked = 0;
			// 判断是否选对答案
			Exam_Result er = null;
			if (dxHasflase(dxquestions.get(key).toString())) {
				er = eqResult.getByFirstNameValue("id", dxquestions.get(key));
				checked = 1;
			}
			// 未保存过，直接保存
			if (upq == null) {
				upq = new Exam_upq();
				upq.set("questionid", strQint(key));
				upq.set("useranswer", dxquestions.get(key).toString());
				upq.set("questiontypeid", 1);
				upq.set("userpaperid", ep.get("id"));
				// er==null 即是没有选择答案
				if (er != null) {
					upq.set("checked", checked);
					if (er.getInt("rightr") == 1) {
						// 此处默认单选得分2分
						// 得分分数
						upq.set("goal", dxgoal);
						// 总分相加
						fraction += dxgoal;
					}

				}

				upq.save();
			} else
			// 已经保存过，更新
			{
				if (er != null) {
					upq.set("checked", checked);
					upq.set("useranswer", dxquestions.get(key).toString());
					if (er.getInt("rightr") == 1) {
						// 此处默认单选得分2分
						// 获取题型分数
						upq.set("goal", dxgoal);
						fraction += dxgoal;
					} else {
						upq.set("goal", 0);
					}
					upq.update();
				}
			}
		}
		// 多选
		JSONObject dxxquestions = (JSONObject) map.get("dxxquestions");
		keyset = dxxquestions.keySet();
		for (String key : keyset) {
			Exam_upq upq = eqUpq.findFirst("select * from exam_upq where questionid='" + strQint(key)
					+ "' and userpaperid='" + ep.getInt("id") + "'");
			if (upq == null) {
				upq = new Exam_upq();
				upq.set("questionid", strQint(key));
				upq.set("useranswer", dxxquestions.get(key).toString());
				upq.set("questiontypeid", 2);
				upq.set("userpaperid", ep.get("id"));
				List lists = (List) dxxquestions.get(key);
				// 查询问题对应的正确答案
				List<Exam_Result> erlist = eqResult
						.find("select * from exam_result where questionid='" + strQint(key) + "' and rightr='1'");
				// 所选的答案数与正确答案数相同才进行判断,其他都直接是答案
				if (erlist.size() == lists.size()) {
					int[] answer = new int[erlist.size()];
					int[] useranswer = new int[erlist.size()];
					for (int i = 0; i < erlist.size(); i++) {
						answer[i] = erlist.get(i).get("id");
						useranswer[i] = Integer.valueOf(lists.get(i).toString());
					}
					Arrays.sort(answer);
					Arrays.sort(useranswer);
					if (Arrays.equals(answer, useranswer)) {
						upq.set("goal", dxxgoal);
						fraction += dxxgoal;
					} else {
						upq.set("goal", 0);
					}

				} else {
					upq.set("goal", 0);
				}

				if (lists.size() > 0) {
					upq.set("checked", 1);
					upq.set("useranswer", dxxquestions.get(key).toString());
				} else {
					upq.set("useranswer", null);
				}
				// System.out.println("dxxsave:" + upq);
				upq.save();
			} else {
				// 用户所答
				List lists = (List) dxxquestions.get(key);
				// 查询问题对应的正确答案
				List<Exam_Result> erlist = eqResult
						.find("select * from exam_result where questionid='" + strQint(key) + "' and rightr='1'");
				// 所选的答案数与正确答案数相同才进行判断,其他都直接是答案
				if (erlist.size() == lists.size()) {
					int[] answer = new int[erlist.size()];
					int[] useranswer = new int[erlist.size()];
					for (int i = 0; i < erlist.size(); i++) {
						answer[i] = erlist.get(i).get("id");
						useranswer[i] = Integer.valueOf(lists.get(i).toString());
					}
					Arrays.sort(answer);
					Arrays.sort(useranswer);
					if (Arrays.equals(answer, useranswer)) {
						upq.set("goal", dxxgoal);
						fraction += dxxgoal;
					} else {
						upq.set("goal", 0);
					}

				} else {
					upq.set("goal", 0);
				}
				if (lists.size() > 0) {
					upq.set("checked", 1);
					upq.set("useranswer", dxxquestions.get(key).toString());

				} else {
					upq.set("useranswer", null);
				}
				// System.out.println("dxxupdate:" + upq);
				upq.update();
			}

		}
		// 判断
		JSONObject pdquestions = (JSONObject) map.get("pdquestions");
		keyset = pdquestions.keySet();
		for (String key : keyset) {
			Exam_upq upq = eqUpq.findFirst("select * from exam_upq where questionid='" + strQint(key)
					+ "' and userpaperid='" + ep.getInt("id") + "'");
			int checked = 0;
			// 判断是否选对答案
			Exam_Result er = null;
			if (dxHasflase(pdquestions.get(key).toString())) {
				er = eqResult.getByFirstNameValue("id", pdquestions.get(key));
				checked = 1;
			}
			// 未保存过，直接保存
			if (upq == null) {
				upq = new Exam_upq();
				upq.set("questionid", strQint(key));
				upq.set("useranswer", pdquestions.get(key).toString());
				upq.set("questiontypeid", 4);
				upq.set("userpaperid", ep.get("id"));
				// er==null 即是没有选择答案
				if (er != null) {
					upq.set("checked", checked);
					if (er.getInt("rightr") == 1) {
						// 此处默认单选得分2分
						// 得分分数
						upq.set("goal", dxgoal);
						// 总分相加
						fraction += dxgoal;
					}

				}

				upq.save();
			} else
			// 已经保存过，更新
			{
				if (er != null) {
					upq.set("checked", checked);
					upq.set("useranswer", pdquestions.get(key).toString());
					if (er.getInt("rightr") == 1) {
						// 此处默认单选得分2分
						// 获取题型分数
						upq.set("goal", pdgoal);
						fraction += pdgoal;
					} else {
						upq.set("goal", 0);
					}
					upq.update();
				}
			}
		}
		// 填空
		JSONObject tkquestions = (JSONObject) map.get("tkquestions");
		keyset = tkquestions.keySet();
		for (String key : keyset) {
			Exam_upq upq = eqUpq.findFirst("select * from exam_upq where questionid='" + strQint(key)
					+ "' and userpaperid='" + ep.getInt("id") + "'");
			if (upq == null) {
				upq = new Exam_upq();
				upq.set("questionid", strQint(key));
				upq.set("questiontypeid", 3);
				upq.set("userpaperid", ep.get("id"));
				upq.set("useranswer", tkquestions.get(key).toString());
				upq.save();
			} else {
				upq.set("useranswer", tkquestions.get(key).toString());
				upq.update();
			}
		}
		ep.set("fraction", fraction);
		ep.update();

		return ep;

	}

	@Deprecated
	public Exam_UserPaper savePaper(Map map) {
		// 用户试卷的部分信息
		JSONObject userpaper = (JSONObject) map.get("userpaper");

		String detailsql = "SELECT examdetails.* FROM exam_exam as exam "
				+ " LEFT JOIN exam_examdetails as examdetails ON" + " examdetails.id=exam.examdetailsid WHERE exam.id='"
				+ userpaper.get("examid") + "'";
		// 获取题型分数
		Exam_ExamDetails ed = eqDetails.findFirst(detailsql);
		int dxgoal = ed.getInt("dxcustomscore");
		int dxxgoal = ed.getInt("dxxcustomscore");
		int tkgoal = ed.getInt("tkcustomscore");
		int pdgoal = ed.getInt("pdcustomscore");
		Exam_UserPaper ep = eqUserPaper.findFirst("select * from exam_userpaper where workno='"
				+ userpaper.get("workno") + "'and display='1' and examid='" + userpaper.get("examid") + "'");
		// 判断用户是否已经参加过考试
		if (ep == null) {
			// 先保存userpaper
			eqUserPaper.clear();
			Map usermap = FastJsonUilt.convertJsonStrToMap(userpaper.toString());
			usermap.put("submitdate", new Date());
			eqUserPaper._setAttrs(usermap);
			eqUserPaper.save();
			ep = eqUserPaper;
		} else {
			Map usermap = FastJsonUilt.convertJsonStrToMap(userpaper.toString());
			usermap.put("submitdate", new Date());
			ep._setAttrs(usermap);
			ep.update();
		}
		// 试卷分数
		int fraction = 0;
		// 单选
		JSONObject dxquestions = (JSONObject) map.get("dxquestions");
		Set<String> keyset = dxquestions.keySet();
		for (String key : keyset) {
			Exam_upq upq = eqUpq.findFirst("select * from exam_upq where questionid='" + strQint(key)
					+ "' and userpaperid='" + ep.getInt("id") + "'");
			int checked = 0;
			// 判断是否选对答案
			Exam_Result er = null;
			if (dxHasflase(dxquestions.get(key).toString())) {
				er = eqResult.getByFirstNameValue("id", dxquestions.get(key));
				checked = 1;
			}
			// 未保存过，直接保存
			if (upq == null) {
				upq = new Exam_upq();
				upq.set("questionid", strQint(key));
				upq.set("useranswer", dxquestions.get(key).toString());
				upq.set("questiontypeid", 1);
				upq.set("userpaperid", ep.get("id"));
				// er==null 即是没有选择答案
				if (er != null) {
					upq.set("checked", checked);
					if (er.getInt("rightr") == 1) {
						// 此处默认单选得分2分
						// 得分分数
						upq.set("goal", dxgoal);
						// 总分相加
						fraction += dxgoal;
					}

				}

				upq.save();
			} else
			// 已经保存过，更新
			{
				if (er != null) {
					upq.set("checked", checked);
					upq.set("useranswer", dxquestions.get(key).toString());
					if (er.getInt("rightr") == 1) {
						// 此处默认单选得分2分
						// 获取题型分数
						upq.set("goal", dxgoal);
						fraction += dxgoal;
					} else {
						upq.set("goal", 0);
					}
					upq.update();
				}
			}
			// System.out.println("dx:" + upq);
		}
		// 多选
		JSONObject dxxquestions = (JSONObject) map.get("dxxquestions");
		keyset = dxxquestions.keySet();
		for (String key : keyset) {
			Exam_upq upq = eqUpq.findFirst("select * from exam_upq where questionid='" + strQint(key)
					+ "' and userpaperid='" + ep.getInt("id") + "'");
			if (upq == null) {
				upq = new Exam_upq();
				upq.set("questionid", strQint(key));
				upq.set("useranswer", dxxquestions.get(key).toString());
				upq.set("questiontype", 2);
				upq.set("userpaperid", ep.get("id"));
				List lists = (List) dxxquestions.get(key);
				// 查询问题对应的正确答案
				List<Exam_Result> erlist = eqResult
						.find("select * from exam_result where questionid='" + strQint(key) + "' and rightr='1'");
				// 所选的答案数与正确答案数相同才进行判断,其他都直接是答案
				if (erlist.size() == lists.size()) {
					int[] answer = new int[erlist.size()];
					int[] useranswer = new int[erlist.size()];
					for (int i = 0; i < erlist.size(); i++) {
						answer[i] = erlist.get(i).get("id");
						useranswer[i] = Integer.valueOf(lists.get(i).toString());
					}
					Arrays.sort(answer);
					Arrays.sort(useranswer);
					if (Arrays.equals(answer, useranswer)) {
						upq.set("goal", dxxgoal);
						fraction += dxxgoal;
					} else {
						upq.set("goal", 0);
					}

				} else {
					upq.set("goal", 0);
				}

				if (lists.size() > 0) {
					upq.set("checked", 1);
					upq.set("useranswer", dxxquestions.get(key).toString());
				} else {
					upq.set("useranswer", null);
				}
				// System.out.println("dxxsave:" + upq);
				upq.save();
			} else {
				// 用户所答
				List lists = (List) dxxquestions.get(key);
				// 查询问题对应的正确答案
				List<Exam_Result> erlist = eqResult
						.find("select * from exam_result where questionid='" + strQint(key) + "' and rightr='1'");
				// 所选的答案数与正确答案数相同才进行判断,其他都直接是答案
				if (erlist.size() == lists.size()) {
					int[] answer = new int[erlist.size()];
					int[] useranswer = new int[erlist.size()];
					for (int i = 0; i < erlist.size(); i++) {
						answer[i] = erlist.get(i).get("id");
						useranswer[i] = Integer.valueOf(lists.get(i).toString());
					}
					Arrays.sort(answer);
					Arrays.sort(useranswer);
					if (Arrays.equals(answer, useranswer)) {
						upq.set("goal", dxxgoal);
						fraction += dxxgoal;
					} else {
						upq.set("goal", 0);
					}

				} else {
					upq.set("goal", 0);
				}
				if (lists.size() > 0) {
					upq.set("checked", 1);
					upq.set("useranswer", dxxquestions.get(key).toString());

				} else {
					upq.set("useranswer", null);
				}
				// System.out.println("dxxupdate:" + upq);
				upq.update();
			}

		}
		// 判断
		JSONObject pdquestions = (JSONObject) map.get("pdquestions");
		keyset = pdquestions.keySet();
		for (String key : keyset) {
			Exam_upq upq = eqUpq.findFirst("select * from exam_upq where questionid='" + strQint(key)
					+ "' and userpaperid='" + ep.getInt("id") + "'");
			int checked = 0;
			// 判断是否作答正确
			Exam_Question eq = null;
			if (dxHasflase(pdquestions.get(key).toString())) {
				eq = eqQuestion.findFirst("select * from exam_question where id='" + strQint(key) + "' and pdright='"
						+ pdquestions.get(key) + "'");
				checked = 1;
			}
			if (upq == null) {
				upq = new Exam_upq();
				upq.set("questionid", strQint(key));
				upq.set("questiontype", 4);
				upq.set("userpaperid", ep.get("id"));
				upq.set("useranswer", pdquestions.get(key).toString());
				if (eq == null) {
					upq.set("goal", 0);
				} else {
					// 获取题型分数
					upq.set("goal", pdgoal);
					fraction += pdgoal;
				}
				upq.save();
			} else {
				if (eq == null) {
					upq.set("goal", 0);
				} else {
					// 获取题型分数
					upq.set("goal", pdgoal);
					fraction += pdgoal;
				}
				upq.set("useranswer", pdquestions.get(key).toString());
				upq.set("checked", checked);
				upq.update();

			}
		}
		// 填空
		JSONObject tkquestions = (JSONObject) map.get("tkquestions");
		keyset = tkquestions.keySet();
		for (String key : keyset) {
			Exam_upq upq = eqUpq.findFirst("select * from exam_upq where questionid='" + strQint(key)
					+ "' and userpaperid='" + ep.getInt("id") + "'");
			if (upq == null) {
				upq = new Exam_upq();
				upq.set("questionid", strQint(key));
				upq.set("questiontype", 3);
				upq.set("userpaperid", ep.get("id"));
				upq.set("useranswer", tkquestions.get(key).toString());
				upq.save();
			} else {
				upq.set("useranswer", tkquestions.get(key).toString());
				upq.update();
			}
		}
		ep.set("fraction", fraction);
		ep.update();

		return ep;
	}

	private int strQint(String str) {
		String regEx = "[^0-9]";
		Pattern p = Pattern.compile(regEx);
		Matcher m = p.matcher(str);
		return Integer.valueOf(m.replaceAll("").trim());
	}

	private boolean dxHasflase(String str) {
		return str.matches("[0-9]{1,}");
	}

	public List<ExamsUserPapers> getExams(Map map) {
		Object workno = map.get("workno");
		String sqlPara = "select * from exam_exam where invalid='1' and stime<='"
				+ DateUtil.getDateTimeFormat(new Date()) + "'<=etime";
		// String sqlPara="select * from exam_exam";
		List<Exam_Exam> ableExam = eqExam.find(sqlPara);
		List<ExamsUserPapers> redata = new ArrayList<ExamsUserPapers>();
		for (Exam_Exam ee : ableExam) {
			List<Exam_UserPaper> eu = eqUserPaper.find("select * from exam_userpaper where workno='" + workno + "'"
					+ " and examid='" + ee.get("id") + "'");
			ExamsUserPapers eup = new ExamsUserPapers();
			Exam_Questiontype eqt = eqQt.findById(ee.get("typeid"));
			eup.setExam(ee);
			eup.setUserpaper(eu);
			eup.setEqt(eqt);
			int limit = ee.get("limit");
			if (limit == eu.size()) {
				int finish = eu.get(eu.size() - 1).get("finish");
				int display = eu.get(eu.size() - 1).get("display");
				if (finish < 100 && display == 1) {
					eup.setLimit(true);
				}
			} else if (eu.size() >= 1 && eu.size() <= limit) {
				eup.setLimit(true);
			} else if (eu.size() == 0) {
				eup.setLimit(true);
			} else {
				eup.setLimit(false);
			}
			redata.add(eup);
		}
		return redata;
	}

	public List<ExamsUserPapers> getExamsMobile(Map map) {
		Object workno = map.get("workno");
		String sqlPara = "select * from exam_exam where invalid='1' and stime<='"
				+ DateUtil.getDateTimeFormat(new Date()) + "'<=etime";
		sqlPara += " and name like '%" + map.get("searchvalue") + "%'";
		// String sqlPara="select * from exam_exam";
		List<Exam_Exam> ableExam = eqExam.find(sqlPara);
		List<ExamsUserPapers> redata = new ArrayList<ExamsUserPapers>();
		for (Exam_Exam ee : ableExam) {
			List<Exam_UserPaper> eu = eqUserPaper.find("select * from exam_userpaper where workno='" + workno + "'"
					+ " and examid='" + ee.get("id") + "'");
			Record sp = Db.findFirst(
					"SELECT sub.name as subname,sub.id as subid,pro.name as proname,pro.id as proid FROM exam_subject sub"
							+ " INNER JOIN exam_project pro  ON sub.id=pro.subjectid WHERE pro.id='"
							+ ee.get("projectid") + "'");
			ExamsUserPapers eup = new ExamsUserPapers();
			Exam_Questiontype eqt = eqQt.findById(ee.get("typeid"));
			eup.setExam(ee);
			eup.setUserpaper(eu);
			eup.setEqt(eqt);
			eup.setSubpro(sp);
			int limit = ee.get("limit");
			if (limit == eu.size()) {
				int finish = eu.get(eu.size() - 1).get("finish");
				int display = eu.get(eu.size() - 1).get("display");
				if (finish < 100 && display == 1) {
					eup.setLimit(true);
				}
			} else if (eu.size() >= 1 && eu.size() <= limit) {
				eup.setLimit(true);
			} else if (eu.size() == 0) {
				eup.setLimit(true);
			} else {
				eup.setLimit(false);
			}
			redata.add(eup);
		}
		return redata;
	}

	/**
	 * H5版查询考试成绩
	 * 
	 * @param map
	 * @return
	 */
	public List<cxExamInfo> getCxExamsMobile(Map map) {
		Object workno = map.get("workno");
		String sqlPara = "SELECT  DISTINCT exam.id, exam.* FROM exam_exam as exam "
				+ "LEFT JOIN exam_userpaper as userpaper ON "
				+ "userpaper.examid=exam.id WHERE exam.display='1' and userpaper.workno='" + workno + "'";
		sqlPara += " and exam.name like '%" + map.get("searchvalue") + "%'";
		// String sqlPara="select * from exam_exam";
		List<Exam_Exam> ableExam = eqExam.find(sqlPara);
		List<cxExamInfo> redata = new ArrayList<cxExamInfo>();
		for (Exam_Exam ee : ableExam) {
			List<Exam_UserPaper> eu = eqUserPaper.find("select * from exam_userpaper where workno='" + workno + "'"
					+ " and examid='" + ee.get("id") + "'");
			Record sp = Db.findFirst(
					"SELECT sub.name as subname,sub.id as subid,pro.name as proname,pro.id as proid FROM exam_subject sub"
							+ " INNER JOIN exam_project pro  ON sub.id=pro.subjectid WHERE pro.id='"
							+ ee.get("projectid") + "'");
			cxExamInfo eup = new cxExamInfo();
			String sql = "SELECT DISTINCT max(fraction) as maxfraction  FROM exam_userpaper as userpaper"
					+ "  WHERE userpaper.workno='" + workno + "' AND userpaper.examid='" + ee.get("id") + "'";
			eup.setExam(ee);
			eup.setSubpro(sp);
			eup.setMaxFraction(Db.findFirst(sql).get("maxfraction").toString());
			eup.setDoExamSize(String.valueOf(eu.size()));
			redata.add(eup);
		}
		return redata;
	}

	public class cxExamInfo {
		private Exam_Exam exam;
		private String doExamSize;
		private String maxFraction;
		private Record subpro;

		public Exam_Exam getExam() {
			return exam;
		}

		public void setExam(Exam_Exam exam) {
			this.exam = exam;
		}

		public String getDoExamSize() {
			return doExamSize;
		}

		public void setDoExamSize(String doExamSize) {
			this.doExamSize = doExamSize;
		}

		public String getMaxFraction() {
			return maxFraction;
		}

		public void setMaxFraction(String maxFraction) {
			this.maxFraction = maxFraction;
		}

		public Record getSubpro() {
			return subpro;
		}

		public void setSubpro(Record subpro) {
			this.subpro = subpro;
		}

	}

	public class ExamsUserPapers {
		private Exam_Exam exam;
		private List<Exam_UserPaper> userpaper;
		private Exam_Questiontype eqt;
		private Record subpro;
		private boolean limit = false;

		public Record getSubpro() {
			return subpro;
		}

		public boolean isLimit() {
			return limit;
		}

		public void setLimit(boolean limit) {
			this.limit = limit;
		}

		public void setSubpro(Record subpro) {
			this.subpro = subpro;
		}

		public Exam_Exam getExam() {
			return exam;
		}

		public void setExam(Exam_Exam exam) {
			this.exam = exam;
		}

		public List<Exam_UserPaper> getUserpaper() {
			return userpaper;
		}

		public void setUserpaper(List<Exam_UserPaper> userpaper) {
			this.userpaper = userpaper;
		}

		public Exam_Questiontype getEqt() {
			return eqt;
		}

		public void setEqt(Exam_Questiontype eqt) {
			this.eqt = eqt;
		}

	}

	public Map hasResult(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Object userpaperid = map.get("id");
		Object examid = map.get("examid");
		Object workno = map.get("workno");
		Exam_UserPaper eup = eqUserPaper.findById(userpaperid);
		Exam_Exam ee = eqExam.findById(examid);
		Exam_User eu = eqUser.getByFirstNameValue("workno", String.valueOf(workno));
		remap.put("titile", ee.get("title"));
		remap.put("name", eu.get("name"));
		remap.put("fraction", eup.get("fraction"));
		int hc = ee.get("hascompletion");
		if (hc == 1) {
			remap.put("hascompletion", "需要人工改卷");
			remap.put("result", "待人工改卷");
		} else {
			remap.put("hascompletion", "自动改卷");
			remap.put("result", "此卷分数为最终分数");
		}
		return remap;
	}

	
	public Map RandomQuestionV2(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		// 查看是否存在未完成的试卷
		Exam_UserPaper ep = eqUserPaper
				.findFirst("select * from exam_userpaper where  finish<='100' and display='1' and workno='"
						+ map.get("workno") + "' and examid='" + map.get("examid") + "'");
		Exam_Exam ee = eqExam.findById(map.get("examid"));
		Exam_ExamDetails eed = eqDetails.findById(ee.get("examdetailsid"));
		// ep==null获取新题目
		if (ep == null) {
			List<PaperQuestionsResult> quesitons=new ArrayList<PaperQuestionsResult>();
			quesitons.addAll(typeQestion("1", eed.getInt("dxsize"), ee.get("subproid")));
			quesitons.addAll(typeQestion("2", eed.getInt("dxxsize"), ee.get("subproid")));
			quesitons.addAll(typeQestion("3", eed.getInt("tksize"), ee.get("subproid")));
			quesitons.addAll(typeQestion("4", eed.getInt("pdsize"), ee.get("subproid")));
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("list", quesitons);
			jsonObject.put("exam", ee);
			jsonObject.put("userpaper", null);
			jsonObject.put("eed", eed);
			remap.put("result", true);
			remap.put("error", "");
			remap.put("data", jsonObject);
		} else {
			List<PaperQuestionsResult> quesitons=new ArrayList<PaperQuestionsResult>();
			List<Exam_upq> upqs = eqUpq.find("select * from exam_upq where userpaperid='" + ep.get("id") + "'");
			for (Exam_upq upq : upqs) {
				int type = upq.getInt("questiontypeid");
				Exam_Question eq = eqQuestion.findFirst("select * from exam_question where id='"
						+ upq.getStr("questionid") + "' and typeid='" + type + "'");
				String rSql = "select * from exam_result WHERE questionid='" + eq.getInt("id") + "'";
				Exam_Questiontype eqt = eqQt.findById(eq.get("typeid"));
				List<Exam_Result> ers = eqResult
						.find("select * from exam_result where questionid='" + eq.get("id") + "'");
				PaperQuestionsResult pqr = new PaperQuestionsResult();
				pqr.setQuestion(eq);
				pqr.setResults(ers);
				pqr.setUpq(upq);
				pqr.setEqt(eqt);
				quesitons.add(pqr);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("list", quesitons);
			jsonObject.put("userpaper", ep);
			jsonObject.put("exam", ee);
			jsonObject.put("eed", eed);
			remap.put("result", true);
			remap.put("error", "");
			remap.put("data", jsonObject);
		}
		return remap;
	}
	
	
	
	
	
	@Deprecated
	public Map RandomQuestion(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		// 查看是否存在未完成的试卷
		Exam_UserPaper ep = eqUserPaper
				.findFirst("select * from exam_userpaper where  finish<='100' and display='1' and workno='"
						+ map.get("workno") + "' and examid='" + map.get("examid") + "'");
		Exam_Exam ee = eqExam.findById(map.get("examid"));
		Exam_ExamDetails eed = eqDetails.findById(ee.get("examdetailsid"));
		// ep==null获取新题目
		if (ep == null) {
			List<PaperQuestionsResult> dx = typeQestion("1", eed.getInt("dxsize"), ee.get("projectid"));
			List<PaperQuestionsResult> dxx = typeQestion("2", eed.getInt("dxxsize"), ee.get("projectid"));
			List<PaperQuestionsResult> pd = typeQestion("4", eed.getInt("pdsize"), ee.get("projectid"));
			List<PaperQuestionsResult> tk = typeQestion("3", eed.getInt("tksize"), ee.get("projectid"));
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("dxquestion", dx);
			jsonObject.put("dxxquestion", dxx);
			jsonObject.put("pdquestion", pd);
			jsonObject.put("tkquestion", tk);
			jsonObject.put("upqs", null);
			jsonObject.put("exam", ee);
			jsonObject.put("userpaper", null);
			jsonObject.put("eed", eed);
			remap.put("result", true);
			remap.put("error", "");
			remap.put("data", jsonObject);
		} else {
			List<PaperQuestionsResult> dx = new ArrayList<PaperQuestionsResult>();
			List<PaperQuestionsResult> dxx = new ArrayList<PaperQuestionsResult>();
			List<PaperQuestionsResult> pd = new ArrayList<PaperQuestionsResult>();
			List<PaperQuestionsResult> tk = new ArrayList<PaperQuestionsResult>();
			List<Exam_upq> upqs = eqUpq.find("select * from exam_upq where userpaperid='" + ep.get("id") + "'");
			for (Exam_upq upq : upqs) {
				int type = upq.getInt("questiontypeid");
				Exam_Question eq = eqQuestion.findFirst("select * from exam_question where id='"
						+ upq.getStr("questionid") + "' and typeid='" + type + "'");
				String rSql = "select * from exam_result WHERE questionid='" + eq.getInt("id") + "'";
				Exam_Questiontype eqt = eqQt.findById(eq.get("typeid"));
				List<Exam_Result> ers = eqResult
						.find("select * from exam_result where questionid='" + eq.get("id") + "'");
				PaperQuestionsResult pqr = new PaperQuestionsResult();
				pqr.setQuestion(eq);
				pqr.setResults(ers);
				pqr.setEqt(eqt);
				if (type == 1) {
					dx.add(pqr);
				} else if (type == 2) {
					dxx.add(pqr);
				} else if (type == 3) {
					tk.add(pqr);
				} else if (type == 4) {
					pd.add(pqr);
				}
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("dxquestion", dx);
			jsonObject.put("dxxquestion", dxx);
			jsonObject.put("pdquestion", pd);
			jsonObject.put("tkquestion", tk);
			jsonObject.put("upqs", upqs);
			jsonObject.put("userpaper", ep);
			jsonObject.put("exam", ee);
			jsonObject.put("eed", eed);
			remap.put("result", true);
			remap.put("error", "");
			remap.put("data", jsonObject);
		}
		return remap;
	}

	private String[] haveCheckVaules(String vaule) {
		String rs = "";
		if (!vaule.equals("")) {
			rs = vaule.substring(1, vaule.length() - 1);
		}
		String[] rss = rs.split(",");
		return rss;
	}

	// 分页获取考试
	public Map getPageCJCX(Map map) {
		return ServiceReturnUilt.ServiceReturn(true, eqExam.pages(map));
	}

	// 获取考试成绩
	public Map getKSCJ(Map map) {
		Object workno = map.get("workno");
		Object examid = map.get("examid");
		List<Exam_UserPaper> eu = eqUserPaper
				.find("select * from exam_userpaper where workno='" + workno + "'" + " and examid='" + examid + "'");
		String sql = "SELECT DISTINCT max(fraction) as maxfraction  FROM exam_userpaper as userpaper"
				+ "  WHERE userpaper.workno='" + workno + "' AND userpaper.examid='" + examid + "'";
		CxInfo info = new CxInfo();
		if (eu != null && eu.size() > 0) {
			info.setMaxFraction(Db.findFirst(sql).get("maxfraction").toString());
			info.setDoExamSize(String.valueOf(eu.size()));
		}
		return ServiceReturnUilt.ServiceReturn(true, info);
	}

	public class CxInfo {
		private String doExamSize = "0";
		private String maxFraction = "0";

		public String getDoExamSize() {
			return doExamSize;
		}

		public void setDoExamSize(String doExamSize) {
			this.doExamSize = doExamSize;
		}

		public String getMaxFraction() {
			return maxFraction;
		}

		public void setMaxFraction(String maxFraction) {
			this.maxFraction = maxFraction;
		}

	}
	/**
	 * 获取用户某一考试中的所有试卷基本信息
	 * 
	 * @param map
	 * @return
	 */
	public Map getExamPaperInfo(Map map) {
		Page page = recordFront.userPaperPage(map);
		return ServiceReturnUilt.ServiceReturnRecordPage(page);
	}
    
	public Map backViewPaper(Map map) {
		return  recordFront.backViewPaper(map);
	}
}
