package examonline.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.jfinal.plugin.activerecord.Db;

import examonline.model.Exam_Exam;
import examonline.model.Exam_ExamDetails;
import examonline.model.Exam_Question;
import examonline.model.Exam_Questiontype;
import examonline.model.Exam_Result;
import examonline.model.Exam_User;
import examonline.model.Exam_UserPaper;
import examonline.model.Exam_upq;
import examonline.service.ExamService.PaperQuestionsResultMobile;
import examonline.uilt.ServiceReturnUilt;
@SuppressWarnings({"rawtypes"})
public class MobileService extends ExamService {

	public Map doLogin(Map map) {
		String sql="select * from exam_user where workno='"+map.get("workno")+"'"
				+" and name='"+map.get("name")+"'";
		Exam_User es=super.eqUser.findFirst(sql);
		if(es==null) {
			return ServiceReturnUilt.ServiceReturnError("用户或密码错误!");
		}
		return ServiceReturnUilt.ServiceReturn(true,es);
}
	public Map RandomQuestion(Map map) {
		
		return super.randomQuestionMobile(map);
	}
	
	
	public Exam_UserPaper savePaper(Map map) {
		return super.savePaperV2(map);
	}
	
	public Map hasResult(Map map) {
		return super.hasResult(map);
		
	}
	
	public Map getCxExams(Map map) {
		return ServiceReturnUilt.ServiceReturn(true, super.getCxExamsMobile(map));
	}
	
	public Map getPageCJCX(Map map) {
		return ServiceReturnUilt.ServiceReturn(true, eqExam.pages(map));
	}
	
	public Map getKSCJ(Map map) {
		Object workno = map.get("workno");
		Object examid = map.get("examid");
		List<Exam_UserPaper> eu = eqUserPaper.find("select * from exam_userpaper where workno='" + workno + "'"
				+ " and examid='" + examid + "'");
		String sql = "SELECT DISTINCT max(fraction) as maxfraction  FROM exam_userpaper as userpaper"
				+ "  WHERE userpaper.workno='" + workno + "' AND userpaper.examid='" + examid + "'";
		CxInfo info=new CxInfo();
		if(eu!=null&&eu.size()>0) {
			info.setMaxFraction(Db.findFirst(sql).get("maxfraction").toString());
			info.setDoExamSize(String.valueOf(eu.size()));
		}
		return ServiceReturnUilt.ServiceReturn(true, info);
	}
	
	public Map  getPreBackView(Map map) {
		Object workno = map.get("workno");
		Object examid = map.get("examid");
		List<Exam_UserPaper> eu = eqUserPaper.find("select * from exam_userpaper where workno='" + workno + "'"
				+ " and examid='" + examid + "'");
		return  ServiceReturnUilt.ServiceReturn(true, eu);
	}
	public Map getBackView(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Exam_UserPaper ep = eqUserPaper
				.findFirst("select * from exam_userpaper where  finish='100' and display='0' and workno='"
						+ map.get("workno") + "' and examid='" + map.get("examid") + "'"+" and id='"+map.get("userpaperid")+"'");
		Exam_Exam ee = eqExam.findById(map.get("examid"));
		Exam_ExamDetails eed = eqDetails.findById(ee.get("examdetailsid"));
		
		if (ep != null) {
			List<PaperQuestionsResultMobile> question = new ArrayList<PaperQuestionsResultMobile>();
			List<Exam_upq> upqs = eqUpq.find("select * from exam_upq where userpaperid='" + ep.get("id") + "'");
			for (Exam_upq upq : upqs) {
				int type = upq.getInt("questiontypeid");
				Exam_Question eq = eqQuestion.findFirst("select * from exam_question where id='"
						+ upq.getStr("questionid") + "' and typeid='" + type + "'");
				String rSql = "select * from exam_result WHERE questionid='" + eq.getInt("id") + "'";
				Exam_Questiontype eqt = eqQt.findById(eq.get("typeid"));
				List<Exam_Result> ers = eqResult
						.find("select * from exam_result where questionid='" + eq.get("id") + "'");
				PaperQuestionsResultMobile pqr = new PaperQuestionsResultMobile();
				// 解析符合手机版答案
				if (type == 2) {
					// 多选
					if (upq.get("useranswer") != null) {
						String rs = upq.get("useranswer");
						
						pqr.setUserAnswer(rs);
					}
				} else {
					pqr.setUserAnswer(upq.get("useranswer"));
				}
				pqr.setQuestion(eq);
				pqr.setResults(ers);
				pqr.setEqt(eqt);
				pqr.setUpq(upq);
				question.add(pqr);
			}
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("list", question);
			jsonObject.put("upqs", null);
			jsonObject.put("userpaper", ep);
			jsonObject.put("exam", ee);
			remap.put("eed", eed);
			remap.put("result", true);
			remap.put("error", "");
			remap.put("data", jsonObject);
		}
		return remap;
	}
	public class  CxInfo{
		private String doExamSize;
		private String maxFraction;
		public String getDoExamSize() {
			return doExamSize;
		}
		public void setDoExamSize(String doExamSize) {
			this.doExamSize = doExamSize;
		}
		public String getMaxFraction() {
			return maxFraction;
		}
		public void setMaxFraction(String maxFraction) {
			this.maxFraction = maxFraction;
		}
		
	}
}
