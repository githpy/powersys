package examonline.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

import examonline.interfaces.ModelInterface;
import examonline.interfaces.ServiceInterface;
import examonline.model.Exam_Exam;
import examonline.model.Exam_ExamDetails;
import examonline.model.Exam_Question;
import examonline.model.Exam_Questiontype;
import examonline.model.Exam_Result;
import examonline.model.Exam_User;
import examonline.model.Exam_UserPaper;
import examonline.model.Exam_upq;
import examonline.uilt.ServiceReturnUilt;
@SuppressWarnings("rawtypes")
public class AbstractService implements ServiceInterface {
	ModelInterface IModel;
	Model model;

	public void setModel(ModelInterface model) {
		this.IModel = model;
		this.model = (Model) model;
	}

	@Override
	public Map getByAttrNameValue(String name, String value) {

		return ServiceReturnUilt.ServiceReturn(true, this.IModel.getByAttrNameValue(name, value));
	}

	@Override
	public Map getByFirstNameValue(String name, String value) {
		return ServiceReturnUilt.ServiceReturn(true, this.IModel.getByFirstNameValue(name, value));
	}

	@Override
	public Map getByFirstNameAndUrl(String name, String url) {
		return ServiceReturnUilt.ServiceReturn(true, this.IModel.getByFirstNameAndUrl(name, url));
	}

	@Override
	public Map getAll() {
		return ServiceReturnUilt.ServiceReturn(true, this.IModel.getAll());
	}

	@Override
	public Map getByAttrNameValueLike(String name, String value) {
		return ServiceReturnUilt.ServiceReturn(true, this.IModel.getByAttrNameValueLike(name, value));
	}

	@Override
	public Map page(int pageNumber, int pageSize) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Page page = this.IModel.page(pageNumber, pageSize);
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", page.getList());
		remap.put("recordsTotal", page.getTotalRow());
		remap.put("recordsFiltered", page.getTotalRow());
		return remap;
	}

	@Override
	public Map page(int pageNumber, int pageSize, Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Page page = this.IModel.page(pageNumber, pageSize, map);
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", page.getList());
		remap.put("recordsTotal", page.getTotalRow());
		remap.put("recordsFiltered", page.getTotalRow());
		return remap;
	}

	@Override
	public Map page(Map map) {
		Map<String, Object> remap = new HashMap<String, Object>();
		Page page = this.IModel.page(map);
		remap.put("result", true);
		remap.put("error", "");
		remap.put("data", page.getList());
		remap.put("recordsTotal", page.getTotalRow());
		remap.put("recordsFiltered", page.getTotalRow());
		return remap;
	}

	@Override
	public Map add(Model model) {
		boolean result = model.save();
		return ServiceReturnUilt.ServiceReturn(result, model);
	}

	@Override
	public Map add(Map map) {
		this.model._setAttrs(map);
		boolean result = this.model.save();
		return ServiceReturnUilt.ServiceReturn(result, this.model);
	}

	@Override
	public Map update(Model model) {
		boolean result = model.update();
		return ServiceReturnUilt.ServiceReturn(result, model);
	}

	@Override
	public Map update(Map map) {
		this.model._setAttrs(map);
		boolean result = this.model.update();
		return ServiceReturnUilt.ServiceReturn(result, this.model);
	}

	@Override
	public Map delete(Map map) {
		this.model._setAttrs(map);
		boolean result = this.model.delete();
		return ServiceReturnUilt.ServiceReturn(result, this.model);
	}

	@Override
	public Map delete(Model model) {
		boolean result = model.update();
		return ServiceReturnUilt.ServiceReturn(result, model);
	}

	@Override
	public Map deletById(String id) {
		boolean result = model.deleteById(id);
		return ServiceReturnUilt.ServiceReturn(result, model);
	}

	@Override
	public Map deletByIds(ArrayList<Integer> ids) {
		if (ids.size() > 0) {
			for (Integer id : ids) {
				deletById(String.valueOf(id));
			}
		} else {
			return ServiceReturnUilt.ServiceReturn(false, "系统错误!");
		}
		return ServiceReturnUilt.ServiceReturn(true);
	}

	


	public ModelInterface getIModel() {
		return IModel;
	}

	public void setIModel(ModelInterface iModel) {
		IModel = iModel;
	}

	public Model getModel() {
		return model;
	}

	public void setModel(Model model) {
		this.model = model;
	}
	
}
