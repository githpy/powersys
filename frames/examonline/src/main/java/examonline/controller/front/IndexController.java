package examonline.controller.front;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;

import examonline.service.ExamService;
import examonline.uilt.FastJsonUilt;
import examonline.uilt.ServiceReturnUilt;

public class IndexController extends Controller {

	private ExamService examService = new ExamService();

	public void index() {
		render("/view/front/index.html");
	}

	/**
	 * 获取随机试题
	 */
	public void randomQuestion() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(examService.RandomQuestionV2(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	// 保存或者交卷
	public void savePaper() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(ServiceReturnUilt.ServiceReturn(true, examService.savePaperV2(map)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	// 获取可用考试
	public void hasExam() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(ServiceReturnUilt.ServiceReturn(true, examService.getExams(map)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	public void doLogin() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(examService.AuThUser(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	public void saveUser() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(examService.saveUser(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	}

	public void pageCXCJ() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(examService.getPageCJCX(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public void getKSCJ() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(examService.getKSCJ(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public void doResult() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(ServiceReturnUilt.ServiceReturn(true, (examService.hasResult(map))));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	public void getExamPaperInfo() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(examService.getExamPaperInfo(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 回看试卷
	 */
	public void getBackViewPaper() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(examService.backViewPaper(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}
}
