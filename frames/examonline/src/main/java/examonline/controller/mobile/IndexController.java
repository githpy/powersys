package examonline.controller.mobile;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import com.jfinal.core.Controller;

import examonline.service.MobileService;
import examonline.uilt.FastJsonUilt;
import examonline.uilt.ServiceReturnUilt;

public class IndexController extends Controller {
	private MobileService service = new MobileService();

	public void index() {
		render("/view/mobile/index.html");
	}

	public void doLogin() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.doLogin(map));
			// renderJson(ServiceReturnUilt.ServiceReturn(false));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	public void randomQuestion() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.RandomQuestion(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	// 获取可用考试
	public void hasExam() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(ServiceReturnUilt.ServiceReturn(true, service.getExamsMobile(map)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	// 保存或者交卷
	public void savePaper() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(ServiceReturnUilt.ServiceReturn(true, service.savePaper(map)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	// 考试结果
	public void doResult() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(ServiceReturnUilt.ServiceReturn(true, service.hasResult(map)));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	public void preCXCJ() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.getCxExams(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

	}

	// 查询获取考试相关信息
	public void pageCXCJ() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.getPageCJCX(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	// 获取成绩信息
	public void getKSCJ() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.getKSCJ(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}

	// 获取考试中的相关试卷信息
	public void getPreBackView() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.getPreBackView(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
	
	// 获取考试中的相关试卷信息
	public void getBackView() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.getBackView(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
