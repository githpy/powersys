package examonline.controller.admin;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.jfinal.core.Controller;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.PathKit;

import examonline.service.ExamServiceAdmin;
import examonline.uilt.FastJsonUilt;
import examonline.uilt.ServiceReturnUilt;

public class IndexController extends Controller {
	private ExamServiceAdmin service = new ExamServiceAdmin();
	
	public void index() {
		render("/view/admin/index.html");
		//render("index.html");
	}
	public void getQuestions() {
		String data = getPara("data");

		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(this.service.geteqQuestion(map));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}
	
	public void getExam() {
		String data = getPara("data");

		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(this.service.getExam(map));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}
	
	public void preEdit() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(this.service.getExamDetailsSubPro(map));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	}

	public void getSubPro() {
		renderJson(this.service.getSubPro());
	}
	/**
	 * 分页获取全部考试以及考试下的所有试卷
	 */
	public void getMarking() {
		String data = getPara("data");

		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(this.service.getMarking(map));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	/**
	 * 获取问题及其答案选项
	 */
	public void getPaper() {
		String data = getPara("data");
		renderJson(service.getQuestions(data));
	}

	public void getExamHasPaper() {
		renderJson(this.service.getExamPaper());
	}

	public void saveQuestion() {
		String data = getPara("data");

		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(this.service.saveQuestion(map));

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	public void saveExam() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(this.service.saveExam(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}

	}

	public void saveQuestionByExcel() {
		String json = HttpKit.readData(getRequest());
		JSONObject reqJson = JSON.parseObject(json);
		String tjson=reqJson.get("data").toString();
		 try {
			json = java.net.URLDecoder.decode(tjson, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.saveQuestionByExcelV2(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	
	}
	
	public void actionExam() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.antionExam(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	
	}
	
	/**
	 * 保存或修改工号	
	 */
	public void editSubPro() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.editSubPro(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	
	
	}
	/**
	 * 根据问题ID获取答案
	 */
	public void getResultByQuestionId() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.getResultByQuestionId(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	
	}
	/**
	 * 修改答案
	 */
	
	public void editResult() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.editResult(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	}
	/**
	 * 更新问题
	 */
	public void updateQuestion() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.updateQuestion(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	}
	
	/**
	 * 获取用户试卷信息
	 */
	public void getUserPapers() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.getUserPaper(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	
	}
	
	/**
	 * 获取考试试卷中间表(评分操作)
	 */
	public void getUqpByMarking() {

		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.getUqpByMarking(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	}
	
	public void saveMarking() {

		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.saveMarking(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	}
	public void getUsers() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.getUsers(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	
	}
	
	public void updateUserBymodel() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.upUser(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	}
	public void saveUserBymodel() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.saveUser(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	}
	
	public void doLogin() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.sysDoLogin(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	
	}
	
	public void downModelExcelFile() {
		String fpath = PathKit.getWebRootPath();
		File file=new File(fpath+"/downfile"+"/试题模板.xls");
		renderFile(file);
	}
	
	public void dataExport() {
		String data = getPara("data");
		try {
			String json = java.net.URLDecoder.decode(data, "UTF-8");
			Map map = FastJsonUilt.convertJsonStrToMap(json);
			renderJson(service.dataExport(map));
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			renderJson(ServiceReturnUilt.ServiceReturn(false));
		}
	
	}
}
